#include <iostream>
#include <sstream>
#include <cstdlib>
#include <cassert>
#include <climits>

#include "png++/png.hpp"
#include <Vector3D.h>
#include <XMLSceneParser.h>
#include <handleGraphicsArgs.h>
#include <camera.h>
#include <creators.h>
#include "hitStruct.h"
#include "shape.h"
#include "scene.h"
#include <ctime>
#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include "iostream"
#include "fstream"
#include "globals.h"

#include <mpi.h>


#include <map>
#include <queue>

#define T_MIN 1
#define GRAY_SCALE 50
#define MAX_COLOR_COUNT 100 * 100 * 3

using namespace sivelab;
using namespace vishraylib;
//global variable include globals.h
//may be put it inside scene
GraphicsArgs args;

int min(int x, int y){
	return x<y?x:y;
}

template <class T>
bool displayBfs(T *node, int level)
{

    queue<T*> q;
    q.push(node);

    while (!q.empty())
    {
        T* tmp = q.front();
        q.pop();
				if(tmp->lnode == NULL){
					cout<<tmp->level<<"   "<<tmp->getBoundingBox().getMinPt()<<
												tmp->getBoundingBox().getMaxPt()<<endl;
					cout<<tmp->level+1<<"   "<<tmp->left->getBoundingBox().getMinPt()<<
												tmp->left->getBoundingBox().getMaxPt()<<endl;
					if(tmp->right!=NULL)
					cout<<tmp->level+1<<"   "<<tmp->right->getBoundingBox().getMinPt()<<
												tmp->right->getBoundingBox().getMaxPt()<<endl;
					
				}else
					cout<<tmp->level<<"   "<<tmp->getBoundingBox().getMinPt()<<
												tmp->getBoundingBox().getMaxPt()<<endl;
        if(tmp->lnode != NULL){
            q.push(tmp->lnode);
				}
        if(tmp->rnode != NULL){
            q.push(tmp->rnode);
				}
    }
}

int bboxHits = 0;
int leafHits = 0;
//unsigned size=0;

bool getTile(int& top, int& left, int& bottom, int& right, const int& size, const int&width, const int& height){
	if((right+1) * (bottom+1) == width * height){
		return false;
	}

	if(width == right+1){
			top+=size;
			if(height - bottom-1<size){
				bottom += (height-bottom-1);
			}else{
				bottom+=size;
			}
			left = -size;
			right = -1;
		}
		left = right +1;
		if(width - right-1<size){
			right+=(width - right-1);
		}else{
			right+=size;
		}
		return true;
}

int main(int argc, char *argv[]) {

	int myid, numProcs, len;
	char processorName[MPI_MAX_PROCESSOR_NAME];
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &numProcs);
	MPI_Comm_rank(MPI_COMM_WORLD, &myid);
	MPI_Get_processor_name(processorName, &len);
	//clock_t start;
	//start = clock();
	args.process(argc, argv);


	XMLSceneParser xmlScene;

	xmlScene.registerCallback("camera", new CameraCreator);

	xmlScene.registerCallback("light", new ContainerClass);
	xmlScene.registerCallback("shader", new ContainerClass);
	xmlScene.registerCallback("shape", new ContainerClass);
	xmlScene.registerCallback("texture", new ContainerClass);
	xmlScene.registerCallback("sceneParameters", new ContainerClass);
	xmlScene.registerCallback("instance", new ContainerClass);
	if(args.verbose)
		cout<<"Parsing XML file...";
	if (args.inputFileName != "")
		xmlScene.parseFile(args.inputFileName);
	else {
		std::cerr << "Need input file!" << std::endl;
		//exit(EXIT_FAILURE);
	}

	Scene* scene = Scene::getInstance();
	
	if(args.verbose){
		//cout<<"DONE "<<getElapsedSeconds(start)<<endl;
		scene->print();
	}
	//exit(EXIT_SUCCESS);
	Camera* c = scene->getCameras()[0];
	std::vector<Shape*> shapes = scene->getShapes();
	BvhNode* root;

	//cout<<size<<endl;
	//scene->sortedObjects.reserve(size);	
	
	if(args.splitMethod == "split"){
		if(args.verbose){
				cout<<"Constructing BVH Tree...";
		}
		
		root = new BvhNode(shapes, 0, 0);
		if(args.verbose){
				//cout<<"DONE "<<getElapsedSeconds(start)<<std::endl;
		}
		scene->setRoot(root);
	}

	int w = args.width, h = args.height;

	int size = args.numCpus;
	int tileCount = 4;//the top, left, bottom, right values for a tile
	MPI_Datatype tileDatatype = MPI_INT;
	int tile[4];
	double colorArray[MAX_COLOR_COUNT];
	int colorCount = size * size * 3 + 4;//added 4 for tile bounds
	long numPixelsProcessed = 0;
	int tilesSent=0;
		
	MPI_Datatype colorDatatype = MPI_DOUBLE;
	MPI_Status status;

	if( 0 == myid){
		//map<int, int (*)[4]> tileMap;

		png::image<png::rgb_pixel> *imData = new png::image<png::rgb_pixel>(w, h);
		//send tiles to available workers

		int left = -size;
		int top = 0;
		int right = -1;
		int bottom = size-1;
		//try to send a tile to every process available but that may not
		//be possible then break from the loop	
		for(tilesSent = 0;tilesSent < numProcs - 1; tilesSent++){
			if(!getTile(top, left, bottom, right, size, w, h)){
			//break if there are no more tiles
				MPI_Send(&tile, tileCount, tileDatatype, tilesSent + 1, 0, MPI_COMM_WORLD);
				//break;
			}else{
			//printf("Trying to send tile %d with %d, %d, %d, %d\n", tilesSent , top, left, bottom, right);
			//order is important!!!
			tile[0] = top;
			tile[1] = left;
			tile[2] = bottom;
			tile[3] = right;
			//pushing tile into a map 
			//tileMap[tilesSent + 1] = &tile;
			//numPixelsProcessed +=	(bottom - top +1) * (right - left + 1);
			MPI_Send(&tile, tileCount, tileDatatype, tilesSent + 1, tilesSent + 1, MPI_COMM_WORLD);
			//printf("sent tile %d with %d, %d, %d, %d with tileCount %d\n", tilesSent , top, left, bottom, right, tileCount);
			}
		}

		//listen for results u
		while(numPixelsProcessed != w * h){	
			//printf("waiting to receive message\n");
			MPI_Recv(&colorArray, colorCount,  colorDatatype, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
			//printf("received color for %d from %d\n", status.MPI_TAG , status.MPI_SOURCE);
			//write to image

			int numOfPixelColorsReceived;
			MPI_Get_count(&status, colorDatatype, &numOfPixelColorsReceived);
			numPixelsProcessed+=((numOfPixelColorsReceived-4)/3);//subtracted 4 as the first 4 are tile bounds
			//retrieve the tile and set pixels
			int t_top    = colorArray[0];
			int t_left   = colorArray[1];
			int t_bottom = colorArray[2];
			int t_right  = colorArray[3];
			int index=4;
			//printf("result from %d received tile t=%d, l=%d, b=%d, r=%d\n", status.MPI_SOURCE, t_top, t_left, t_bottom, t_right);
			for(int j=t_left; j<t_left+(t_right - t_left +1);j++)
					for(int k=t_top; k<t_top+(t_bottom - t_top+1);k++){
							//printf("setting color for %d, %d is %f, %f, %f\n", j, k, colorArray[index] * 255, colorArray[index+1] * 255, colorArray[index+2]* 255);
							imData->set_pixel(j , h - k - 1, png::rgb_pixel(
							colorArray[index] * 255,
							colorArray[index+1] * 255,
							colorArray[index+2] * 255));
							index+=3;

					}
			double percent= (numPixelsProcessed*100)/(w * h);
			//cerr<<"\r"<<(int)percent<<"%";
			//send more data
			//printf("number of sent rows %ld\n", numPixels);
			//if(numPixelsProcessed < w * h){
				//printf("rows to be sent exists\n");
				//printf("Trying to send pixel %ld to %d\n", numPixels , status.MPI_SOURCE);
			if(getTile(top, left, bottom, right, size, w, h)){
				tile[0] = top;
				tile[1] = left;
				tile[2] = bottom;
				tile[3] = right;

				//tileMap[tilesSent] = &tile;
				MPI_Send(&tile, tileCount, tileDatatype, status.MPI_SOURCE, tilesSent, MPI_COMM_WORLD);
				//printf("sent pixel %ld to %d\n", numPixels, status.MPI_SOURCE);
				tilesSent++;
			}else{
				//printf("no more pixels to be sent to %d\n", status.MPI_SOURCE);
				MPI_Send(&tile, tileCount, tileDatatype, status.MPI_SOURCE, 0, MPI_COMM_WORLD);
			}
		}
		//cout<<"writing to image file"<<endl;
		string outputFile = "red_DualLoop.png";
		if (args.outputFileName.length() != 0)
			outputFile = args.outputFileName;
		imData->write(outputFile);
		if(args.verbose){
			std::cout<<std::endl<<"No. of threads: "<<args.numCpus<<endl
							<<"rpp: "<<args.rpp<<endl;
							//<<"time: "<<getElapsedSeconds(start)<<endl;
			if(args.splitMethod == "split"){
				std::cout<<"Leaf hits: "<<leafHits<<endl;
				std::cout<<"Boundingbox hits: "<<bboxHits<<endl;
			}
		}
		
		printf("total tiles %d\n", tilesSent);
		//exit(EXIT_SUCCESS);
	}else{
		//printf("worker %d waiting for work\n", myid);
		if( myid > w * h){
			//printf("Worker %d exiting: no work\n", myid);
			MPI_Finalize();
			return 0;
		}
		while(1){
			//receive pixel data
			MPI_Recv(&tile, tileCount, tileDatatype, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
			//process pixel
			if(!status.MPI_TAG){
				printf("worker %d done!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n", myid);
				MPI_Finalize();
				return 0;
			}

			//cout<<"pixel "<<numPixels<<"   "<<x<<","<<y<<" computed at "<<processorName
			//				<<" with id: "<<myid<<endl;	
			int t_top    = tile[0];
			int t_left   = tile[1];
			int t_bottom = tile[2];
			int t_right  = tile[3];
			
			colorArray[0] = t_top;
			colorArray[1] = t_left;
			colorArray[2] = t_bottom;
			colorArray[3] = t_right;
			int index=4;//since we have already pushed 4 bound values into colorArray
			int tileWidth = (t_right - t_left + 1);
			int tileHeight = (t_bottom - t_top + 1);
			int colorOffset;
			//cout<<"tile width: "<<tileWidth<<"\n";
			//cout<<"tile height: "<<tileHeight<<"\n";
			printf("worker %d received tile t=%d, l=%d, b=%d, r=%d\n", myid, t_top, t_left, t_bottom, t_right);
			//while(getTile(t_top, t_left, t_bottom, t_right, 10, tileWidth, tileHeight)){
			//cout<<"current sub tile	t:"<<t_top<<", l: "<<t_left<<", b: "<<t_bottom<<" ,r: "<<t_right<<endl;		
				int tt_left    = -10;
				int tt_top   = 0;
				int tt_right = -1;
				int tt_bottom  = 10-1;

				//while(getTile(tt_top, tt_left, tt_bottom, tt_right, 10, tileWidth, tileHeight)){
				//	cout<<"current sub tile	t:"<<tt_top + t_top<<", l: "<<tt_left + t_left<<", b: "<<tt_bottom + t_top<<" ,r: "<<tt_right + t_left<<endl;		
				//	int sub_left = tt_left + t_left;
				//	int sub_top = tt_top + t_top;
				//	int sub_bottom = tt_bottom + t_top;
				//	int sub_right = tt_right + t_left;

				for(int y=t_top; y<t_top + (t_bottom - t_top + 1) ;y++){
					for(int x=t_left; x<t_left + (t_right - t_left + 1) ;x++){
							//printf("computation for pixel	%d, %d\n", x, y);
							Vector3D pixelColor(0, 0, 0);
							//no computing sqrt pass N directly
							int n = sqrt(args.rpp);
							int N = n*n;
							vector<Vec2D> r;
							vector<Vec2D> s;
							for(int i=0;i<N;i++){
								Vec2D temp;
								temp.x = drand48();
								temp.y = drand48();
								r.push_back(temp);
								temp.x = drand48();
								temp.y = drand48();
								s.push_back(temp);
							}
							random_shuffle(s.begin(), s.end());

							for(int p = 0; p<n;p++){
								for(int q= 0; q<n; q++){
									Ray ray;
									double rand1 = drand48();
									double rand2 = drand48();
									//cout<<(p + rand1)/N<<endl;
									//cout<<(q + rand2)/N<<endl;
									c->computeRay(ray, x+(p + rand1)/n,
																	y + (q + rand2)/n);
									//cout<<ray.getOrigin()<<ray.getDirection()<<endl;
									double tmin = T_MIN;
									double tmax = numeric_limits<double>::max();
									pixelColor += scene->computeRayColor(ray, tmin, tmax, args.recursionDepth, s[p*n+q]);
								}
							}
							
							r.clear();
							s.clear();

							pixelColor*=(1.0/(N));
							pixelColor.clamp(0,1);
							//send color back
							int voffset = y - t_top;
							int hoffset = x -	t_left; 
							colorOffset = hoffset * tileHeight * 3 + voffset * 3 + 4;
							//cout<<"pixel position in colorArray: "<<hoffset * tileHeight * 3 + voffset * 3 + 4<<"\n";

							//cout<<"actual is position is: "<<index<<"\n";
							//index+=3;
							colorArray[index++] = pixelColor[0];
							colorArray[index++] = pixelColor[1];
							colorArray[index++] = pixelColor[2];
							//printf("color for %d, %d is %f, %f, %f\n", x, y, pixelColor[0], pixelColor[1], pixelColor[2]);

						}
					}
			//}
			//cout<<"index: "<<index<<", colorOffset: "<<colorOffset + 3<<endl;
			MPI_Send(&colorArray, index, colorDatatype, 0, status.MPI_TAG, MPI_COMM_WORLD);
			//MPI_Send(&colorArray, index, colorDatatype, 0, status.MPI_TAG, MPI_COMM_WORLD);
			printf("worker %d sending %d results for tile %d, %d, %d, %d\n", myid, index, tile[0], tile[1], tile[2], tile[3]);
		}
	}
	MPI_Finalize();
	return 0;
}

