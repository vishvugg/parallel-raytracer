/* dummy comment to commit correct main*/
#include <iostream>
#include <sstream>
#include <cstdlib>
#include <cassert>
#include <climits>

#include "png.hpp"
#include <Vector3D.h>
#include <XMLSceneParser.h>
#include <handleGraphicsArgs.h>
#include <camera.h>
#include <creators.h>
#include "hitStruct.h"
#include "shape.h"
#include "scene.h"
#include <ctime>
#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include "iostream"
#include "fstream"
#include "globals.h"


#include <queue>

#define T_MIN 1
#define GRAY_SCALE 50
using namespace sivelab;
using namespace vishraylib;
//global variable include globals.h
//may be put it inside scene
GraphicsArgs args;


class threadData{
 public:
	 threadData(int p_threadsRunning):pixelsDone(-args.rpp), 
				threadsRunning(p_threadsRunning)
	 {

	 }
	threadData(threadData& td){}
	void getNoOfReaminingThreads(int& threadsCompleted);
	void stopAThread();
	void  getPixelsDone(unsigned int& pixel);

  	boost::mutex mutex_;
		 unsigned int pixelsDone;
		 unsigned int threadsRunning;
};

			
inline void threadData::getNoOfReaminingThreads(int& threadsCompleted){
		//mutex_.lock();
		threadsCompleted = threadsRunning;
		double percent= (pixelsDone*100)/(args.width*args.height);
		cerr<<"\r"<<(int)percent<<"%";
		//mutex_.unlock();
}

void threadData::stopAThread(){
		mutex_.lock();
		threadsRunning--;
		mutex_.unlock();
}

inline void threadData::getPixelsDone(unsigned int& pixel){
		mutex_.lock();
		pixelsDone+=args.rpp;
		pixel = pixelsDone;
		mutex_.unlock();
}


void processPixel(threadData* tdata,
							Camera *c,
							const	vector<Shape*>& shapes,
							 png::image<png::rgb_pixel> *imData,
							 int threadNum){
		Scene* scene = Scene::getInstance();
	while(true){
		unsigned h = imData->get_height();
		unsigned w = imData->get_width();
		unsigned pixelsDone;
		unsigned count;
		tdata->getPixelsDone(pixelsDone);
		if(pixelsDone>=w*h){
				tdata->stopAThread();
				return;
		}
		if(pixelsDone+args.rpp>=w*h)
						count = w*h - pixelsDone;
		else	
						count = args.rpp;

		for(unsigned i=0;i<count;i++)	{
				size_t x = (pixelsDone+i) % w;
				size_t y = static_cast<size_t>(floor((pixelsDone+i) / static_cast<float>(imData->get_width())) );
				//double tmin = T_MIN;
				//double tmax = numeric_limits<double>::max();
				//	std::cout << std::endl << std::endl << "computation for pixel "
				//			<< y << ", " << x << std::endl;
				//}
				//cout<<"Ray Dir "<<ray.getDirection()<<" Ray Origin "<<ray.getOrigin()<<endl;
				//continue;
				Vector3D pixelColor(0, 0, 0);
				//no computing sqrt pass N directly
				int n = sqrt(args.rpp);
				int N = n*n;
				vector<Vec2D> r;
				vector<Vec2D> s;
				for(int i=0;i<N;i++){
					Vec2D temp;
					temp.x = drand48();
					temp.y = drand48();
					r.push_back(temp);
					temp.x = drand48();
					temp.y = drand48();
					s.push_back(temp);
				}
				random_shuffle(s.begin(), s.end());

				for(int p = 0; p<n;p++){
					for(int q= 0; q<n; q++){
						Ray ray;
						double rand1 = drand48();
						double rand2 = drand48();
						//cout<<(p + rand1)/N<<endl;
						//cout<<(q + rand2)/N<<endl;
						c->computeRay(ray, x+(p + rand1)/n,
														y + (q + rand2)/n);
						//cout<<ray.getOrigin()<<ray.getDirection()<<endl;
						double tmin = T_MIN;
						double tmax = numeric_limits<double>::max();
						pixelColor += scene->computeRayColor(ray, tmin, tmax, args.recursionDepth, s[p*n+q]);
					}
				}
				
				r.clear();
				s.clear();

				pixelColor*=(1.0/(N));
				pixelColor.clamp(0,1);
				printf("setting color for %d, %d is %f, %f, %f\n",x, y, pixelColor[0], pixelColor[1], pixelColor[2]);
					imData->set_pixel(x , h - y - 1, png::rgb_pixel(
							pixelColor[0] * 255,
							pixelColor[1] * 255,
							pixelColor[2] * 255));
			}
	}

	//std::cout<<"Thread done!";
}



template <class T>
bool displayBfs(T *node, int level)
{

    queue<T*> q;
    q.push(node);

    while (!q.empty())
    {
        T* tmp = q.front();
        q.pop();
				if(tmp->lnode == NULL){
					cout<<tmp->level<<"   "<<tmp->getBoundingBox().getMinPt()<<
												tmp->getBoundingBox().getMaxPt()<<endl;
					cout<<tmp->level+1<<"   "<<tmp->left->getBoundingBox().getMinPt()<<
												tmp->left->getBoundingBox().getMaxPt()<<endl;
					if(tmp->right!=NULL)
					cout<<tmp->level+1<<"   "<<tmp->right->getBoundingBox().getMinPt()<<
												tmp->right->getBoundingBox().getMaxPt()<<endl;
					
				}else
					cout<<tmp->level<<"   "<<tmp->getBoundingBox().getMinPt()<<
												tmp->getBoundingBox().getMaxPt()<<endl;
        if(tmp->lnode != NULL){
            q.push(tmp->lnode);
				}
        if(tmp->rnode != NULL){
            q.push(tmp->rnode);
				}
    }
}

int bboxHits = 0;
int leafHits = 0;
//unsigned size=0;

int main(int argc, char *argv[]) {
	clock_t start;
	start = clock();
	args.process(argc, argv);


	XMLSceneParser xmlScene;

	xmlScene.registerCallback("camera", new CameraCreator);

	xmlScene.registerCallback("light", new ContainerClass);
	xmlScene.registerCallback("shader", new ContainerClass);
	xmlScene.registerCallback("shape", new ContainerClass);
	xmlScene.registerCallback("texture", new ContainerClass);
	xmlScene.registerCallback("sceneParameters", new ContainerClass);
	xmlScene.registerCallback("instance", new ContainerClass);
	if(args.verbose)
		cout<<"Parsing XML file...";
	if (args.inputFileName != "")
		xmlScene.parseFile(args.inputFileName);
	else {
		std::cerr << "Need input file!" << std::endl;
		exit(EXIT_FAILURE);
	}

	Scene* scene = Scene::getInstance();
	
	if(args.verbose){
		cout<<"DONE "<<getElapsedSeconds(start)<<endl;
		scene->print();
	}
	//exit(EXIT_SUCCESS);
	Camera* c = scene->getCameras()[0];
	std::vector<Shape*> shapes = scene->getShapes();
	BvhNode* root;

	//cout<<size<<endl;
	//scene->sortedObjects.reserve(size);	
	
	if(args.splitMethod == "split"){
		if(args.verbose){
				cout<<"Constructing BVH Tree...";
		}
		
		root = new BvhNode(shapes, 0, 0);
		if(args.verbose){
				cout<<"DONE "<<getElapsedSeconds(start)<<std::endl;
		}
		scene->setRoot(root);
	}

	int w = args.width, h = args.height;
	png::image<png::rgb_pixel> *imData = new png::image<png::rgb_pixel>(w, h);
	boost::thread_group t_group;
	threadData *tdata = new threadData(args.numCpus);
	for(int i=0; i<args.numCpus;i++){
					//std::cout<<"Thread "<<i+1<<" started!"<<std::endl;
					t_group.create_thread(boost::bind(&processPixel, tdata, c, shapes, imData, i));
	}

	t_group.join_all();
	int remainingThreads;
	//do{
	//	if(!remainingThreads)
	//	std::cout<<remainingThreads;
		//	boost::this_thread::sleep(boost::posix_time::seconds(10));
	//	tdata->getNoOfReaminingThreads(remainingThreads);
	//} while(remainingThreads>0);
	string outputFile = "red_DualLoop.png";
	if (args.outputFileName.length() != 0)
		outputFile = args.outputFileName;
	imData->write(outputFile);
	if(args.verbose){
		std::cout<<std::endl<<"No. of threads: "<<args.numCpus<<endl
						<<"rpp: "<<args.rpp<<endl
						<<"time: "<<getElapsedSeconds(start)<<endl;
		if(args.splitMethod == "split"){
			std::cout<<"Leaf hits: "<<leafHits<<endl;
			std::cout<<"Boundingbox hits: "<<bboxHits<<endl;
		}
	}
	exit(EXIT_SUCCESS);
}

