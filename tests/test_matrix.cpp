#include "matrix.h"
#include "Vector3D.h"

using namespace vishraylib;
int main(int argc, const char *argv[])
{
	vector<double> elements;
	elements.push_back(1);
	elements.push_back(0);
	elements.push_back(0);
	elements.push_back(-2.5);

	elements.push_back(0);
	elements.push_back(2);
	elements.push_back(0);
	elements.push_back(2.0);
	
	elements.push_back(0);
	elements.push_back(0);
	elements.push_back(1);
	elements.push_back(-4.0);
	
	elements.push_back(0);
	elements.push_back(0);
	elements.push_back(0);
	elements.push_back(1);
	Matrix m(4,4,elements);
	cout<<m;
	cout<<m.inverse();
	//cout<<"origional"<<endl<<m;
	//cout<<"reverse"<<endl<<m.inverse();
	return 0;
}
