#include <iostream>
#include <sstream>
#include <cstdlib>
#include <cassert>
#include <climits>

#include "png++/png.hpp"
#include <Vector3D.h>
#include <XMLSceneParser.h>
#include <handleGraphicsArgs.h>
#include <camera.h>
#include <creators.h>
#include "hitStruct.h"
#include "shape.h"
#include "scene.h"
#include <ctime>
#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include "iostream"
#include "fstream"
#include "globals.h"

#include <mpi.h>


#include <queue>

#define T_MIN 1
#define GRAY_SCALE 50
using namespace sivelab;
using namespace vishraylib;
//global variable include globals.h
//may be put it inside scene
GraphicsArgs args;

/*
class threadData{
 public:
	 threadData(int p_threadsRunning):pixelsDone(-args.rpp), 
				threadsRunning(p_threadsRunning)
	 {

	 }
	threadData(threadData& td){}
	void getNoOfReaminingThreads(int& threadsCompleted);
	void stopAThread();
	void  getPixelsDone(unsigned int& pixel);

  	boost::mutex mutex_;
		 unsigned int pixelsDone;
		 unsigned int threadsRunning;
};

			
inline void threadData::getNoOfReaminingThreads(int& threadsCompleted){
		//mutex_.lock();
		threadsCompleted = threadsRunning;
		double percent= (pixelsDone*100)/(args.width*args.height);
		cerr<<"\r"<<(int)percent<<"%";
		//mutex_.unlock();
}

void threadData::stopAThread(){
		mutex_.lock();
		threadsRunning--;
		mutex_.unlock();
}

inline void threadData::getPixelsDone(unsigned int& pixel){
		mutex_.lock();
		pixelsDone+=args.rpp;
		pixel = pixelsDone;
		mutex_.unlock();
}


void processPixel(threadData* tdata,
							Camera *c,
							const	vector<Shape*>& shapes,
							 png::image<png::rgb_pixel> *imData,
							 int threadNum){
		Scene* scene = Scene::getInstance();
	while(true){
		unsigned h = imData->get_height();
		unsigned w = imData->get_width();
		unsigned pixelsDone;
		unsigned count;
		tdata->getPixelsDone(pixelsDone);
		if(pixelsDone>=w*h){
				tdata->stopAThread();
				return;
		}
		if(pixelsDone+args.rpp>=w*h)
						count = w*h - pixelsDone;
		else	
						count = args.rpp;

		for(unsigned i=0;i<count;i++)	{
				size_t x = (pixelsDone+i) % w;
				size_t y = static_cast<size_t>(floor((pixelsDone+i) / static_cast<float>(imData->get_width())) );
				//double tmin = T_MIN;
				//double tmax = numeric_limits<double>::max();
				//	std::cout << std::endl << std::endl << "computation for pixel "
				//			<< y << ", " << x << std::endl;
				//}
				//cout<<"Ray Dir "<<ray.getDirection()<<" Ray Origin "<<ray.getOrigin()<<endl;
				//continue;
				Vector3D pixelColor(0, 0, 0);
				//no computing sqrt pass N directly
				int n = sqrt(args.rpp);
				int N = n*n;
				vector<Vec2D> r;
				vector<Vec2D> s;
				for(int i=0;i<N;i++){
					Vec2D temp;
					temp.x = drand48();
					temp.y = drand48();
					r.push_back(temp);
					temp.x = drand48();
					temp.y = drand48();
					s.push_back(temp);
				}
				random_shuffle(s.begin(), s.end());

				for(int p = 0; p<n;p++){
					for(int q= 0; q<n; q++){
						Ray ray;
						double rand1 = drand48();
						double rand2 = drand48();
						//cout<<(p + rand1)/N<<endl;
						//cout<<(q + rand2)/N<<endl;
						c->computeRay(ray, x+(p + rand1)/n,
														y + (q + rand2)/n);
						//cout<<ray.getOrigin()<<ray.getDirection()<<endl;
						double tmin = T_MIN;
						double tmax = numeric_limits<double>::max();
						pixelColor += scene->computeRayColor(ray, tmin, tmax, args.recursionDepth, s[p*n+q]);
					}
				}
				
				r.clear();
				s.clear();

				pixelColor*=(1.0/(N));
				pixelColor.clamp(0,1);
					imData->set_pixel(x , h - y - 1, png::rgb_pixel(
							pixelColor[0] * 255,
							pixelColor[1] * 255,
							pixelColor[2] * 255));
			}
	}

	//std::cout<<"Thread done!";
}
*/

int min(int x, int y){
	return x<y?x:y;
}

template <class T>
bool displayBfs(T *node, int level)
{

    queue<T*> q;
    q.push(node);

    while (!q.empty())
    {
        T* tmp = q.front();
        q.pop();
				if(tmp->lnode == NULL){
					cout<<tmp->level<<"   "<<tmp->getBoundingBox().getMinPt()<<
												tmp->getBoundingBox().getMaxPt()<<endl;
					cout<<tmp->level+1<<"   "<<tmp->left->getBoundingBox().getMinPt()<<
												tmp->left->getBoundingBox().getMaxPt()<<endl;
					if(tmp->right!=NULL)
					cout<<tmp->level+1<<"   "<<tmp->right->getBoundingBox().getMinPt()<<
												tmp->right->getBoundingBox().getMaxPt()<<endl;
					
				}else
					cout<<tmp->level<<"   "<<tmp->getBoundingBox().getMinPt()<<
												tmp->getBoundingBox().getMaxPt()<<endl;
        if(tmp->lnode != NULL){
            q.push(tmp->lnode);
				}
        if(tmp->rnode != NULL){
            q.push(tmp->rnode);
				}
    }
}

int bboxHits = 0;
int leafHits = 0;
//unsigned size=0;

int main(int argc, char *argv[]) {

	int myid, numProcs, len;
	char processorName[MPI_MAX_PROCESSOR_NAME];
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &numProcs);
	MPI_Comm_rank(MPI_COMM_WORLD, &myid);
	MPI_Get_processor_name(processorName, &len);
	//clock_t start;
	//start = clock();
	args.process(argc, argv);


	XMLSceneParser xmlScene;

	xmlScene.registerCallback("camera", new CameraCreator);

	xmlScene.registerCallback("light", new ContainerClass);
	xmlScene.registerCallback("shader", new ContainerClass);
	xmlScene.registerCallback("shape", new ContainerClass);
	xmlScene.registerCallback("texture", new ContainerClass);
	xmlScene.registerCallback("sceneParameters", new ContainerClass);
	xmlScene.registerCallback("instance", new ContainerClass);
	if(args.verbose)
		cout<<"Parsing XML file...";
	if (args.inputFileName != "")
		xmlScene.parseFile(args.inputFileName);
	else {
		std::cerr << "Need input file!" << std::endl;
		//exit(EXIT_FAILURE);
	}

	Scene* scene = Scene::getInstance();
	
	if(args.verbose){
		//cout<<"DONE "<<getElapsedSeconds(start)<<endl;
		scene->print();
	}
	//exit(EXIT_SUCCESS);
	Camera* c = scene->getCameras()[0];
	std::vector<Shape*> shapes = scene->getShapes();
	BvhNode* root;

	//cout<<size<<endl;
	//scene->sortedObjects.reserve(size);	
	
	if(args.splitMethod == "split"){
		if(args.verbose){
				cout<<"Constructing BVH Tree...";
		}
		
		root = new BvhNode(shapes, 0, 0);
		if(args.verbose){
				//cout<<"DONE "<<getElapsedSeconds(start)<<std::endl;
		}
		scene->setRoot(root);
	}

	int w = args.width, h = args.height;

	int pixelCount = 1;
	MPI_Datatype pixelDatatype = MPI_INT;
	double colorArray[3];
	int colorCount = 3;
	long numPixels = 0;
	MPI_Datatype colorDatatype = MPI_DOUBLE;
	MPI_Status status;

	if( 0 == myid){
		png::image<png::rgb_pixel> *imData = new png::image<png::rgb_pixel>(w, h);
		//send pixel data to available workers
		for(numPixels = 0; numPixels < min( w * h ,numProcs - 1); numPixels++){
			//printf("Trying to send pixel %ld to %ld\n", numPixels , numPixels+1);
			MPI_Send(&numPixels, pixelCount, pixelDatatype, numPixels + 1, numPixels + 1, MPI_COMM_WORLD);
			//printf("sent pixel %ld to %ld\n", numPixels, numPixels+1);
		}
		//listen for results u
		for(int i =0; i< w *h; i++){	
			//printf("waiting to receive message\n");
			MPI_Recv(&colorArray, colorCount,  colorDatatype, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
			//printf("received color for %d from %d\n", status.MPI_TAG , status.MPI_SOURCE);
			//write to image
			size_t x = (status.MPI_TAG-1) % w;
			size_t y = static_cast<size_t>(floor((status.MPI_TAG-1) / static_cast<float>(w) ));
			imData->set_pixel(x , h - y - 1, png::rgb_pixel(
							colorArray[0] * 255,
							colorArray[1] * 255,
							colorArray[2] * 255));
			//send more data
			//printf("number of sent rows %ld\n", numPixels);
			if(numPixels < w * h){
				//printf("rows to be sent exists\n");
				//printf("Trying to send pixel %ld to %d\n", numPixels , status.MPI_SOURCE);
				MPI_Send(&numPixels, pixelCount, pixelDatatype, status.MPI_SOURCE, numPixels, MPI_COMM_WORLD);
				//printf("sent pixel %ld to %d\n", numPixels, status.MPI_SOURCE);
				numPixels++;
			}else{
				//printf("no more pixels to be sent to %d\n", status.MPI_SOURCE);
				MPI_Send(&numPixels, pixelCount, pixelDatatype, status.MPI_SOURCE, 0, MPI_COMM_WORLD);
			}
		}
		//cout<<"writing to image file"<<endl;
		string outputFile = "red_DualLoop.png";
		if (args.outputFileName.length() != 0)
			outputFile = args.outputFileName;
		imData->write(outputFile);
		if(args.verbose){
			std::cout<<std::endl<<"No. of threads: "<<args.numCpus<<endl
							<<"rpp: "<<args.rpp<<endl;
							//<<"time: "<<getElapsedSeconds(start)<<endl;
			if(args.splitMethod == "split"){
				std::cout<<"Leaf hits: "<<leafHits<<endl;
				std::cout<<"Boundingbox hits: "<<bboxHits<<endl;
			}
		}
		//exit(EXIT_SUCCESS);
	}else{
		//printf("worker %d waiting for work\n", myid);
		if( myid > w * h){
			//printf("Worker %d exiting: no work\n", myid);
			MPI_Finalize();
			return 0;
		}
		while(1){
			//receive pixel data
			MPI_Recv(&numPixels, pixelCount, pixelDatatype, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
			//printf("worker %d received pixel %d\n", myid, status.MPI_TAG-1);
			//process pixel
			if(!status.MPI_TAG){
				//printf("worker %d done!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n", myid);
				MPI_Finalize();
				return 0;
			}
			size_t x = (numPixels) % w;
			size_t y = static_cast<size_t>(floor((numPixels) / static_cast<float>(w)));

			//cout<<"pixel "<<numPixels<<"   "<<x<<","<<y<<" computed at "<<processorName
			//				<<" with id: "<<myid<<endl;	
		
			Vector3D pixelColor(0, 0, 0);
			//no computing sqrt pass N directly
			int n = sqrt(args.rpp);
			int N = n*n;
			vector<Vec2D> r;
			vector<Vec2D> s;
			for(int i=0;i<N;i++){
				Vec2D temp;
				temp.x = drand48();
				temp.y = drand48();
				r.push_back(temp);
				temp.x = drand48();
				temp.y = drand48();
				s.push_back(temp);
			}
			random_shuffle(s.begin(), s.end());

			for(int p = 0; p<n;p++){
				for(int q= 0; q<n; q++){
					Ray ray;
					double rand1 = drand48();
					double rand2 = drand48();
					//cout<<(p + rand1)/N<<endl;
					//cout<<(q + rand2)/N<<endl;
					c->computeRay(ray, x+(p + rand1)/n,
													y + (q + rand2)/n);
					//cout<<ray.getOrigin()<<ray.getDirection()<<endl;
					double tmin = T_MIN;
					double tmax = numeric_limits<double>::max();
					pixelColor += scene->computeRayColor(ray, tmin, tmax, args.recursionDepth, s[p*n+q]);
				}
			}
			
			r.clear();
			s.clear();

			pixelColor*=(1.0/(N));
			pixelColor.clamp(0,1);
			//send color back
			colorArray[0] = pixelColor[0];
			colorArray[1] = pixelColor[1];
			colorArray[2] = pixelColor[2];
			MPI_Send(&colorArray, colorCount, colorDatatype, 0, status.MPI_TAG, MPI_COMM_WORLD);
			//printf("sending result for pixel %d\n", status.MPI_TAG-1);
		}
	}
	/*
	boost::thread_group t_group;
	threadData *tdata = new threadData(args.numCpus);
	for(int i=0; i<args.numCpus;i++){
					//std::cout<<"Thread "<<i+1<<" started!"<<std::endl;
					t_group.create_thread(boost::bind(&processPixel, tdata, c, shapes, imData, i));
	}

	t_group.join_all();
	int remainingThreads;
	//do{
	//	if(!remainingThreads)
	//	std::cout<<remainingThreads;
		//	boost::this_thread::sleep(boost::posix_time::seconds(10));
	//	tdata->getNoOfReaminingThreads(remainingThreads);
	//} while(remainingThreads>0);
	*/
	MPI_Finalize();
	return 0;
}

