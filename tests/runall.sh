#!/bin/bash
for scene in SceneFiles_TestA/*.xml
do
  for i in {1..1}
  do
     echo "./main -i $scene -h $1 -w $1 -o $scene.png -n 8 "
	echo $scene
	outputimage=$( basename "$scene" )".png"
	echo $outputimage
     ./main -i $scene -h $1 -w $1 -o $outputimage -n 8
  done
done
