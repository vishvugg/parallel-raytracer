#include "orthoNormalBasis.h"
#include "Vector3D.h"

using namespace vishraylib;
int main(int argc, const char *argv[])
{

	Vector3D a(0,3,4) , d(0,1,0);
	OrthoNormalBasis o(a ,d);
	return 0;
}
