# CMake generated Testfile for 
# Source directory: /home/vishu/workspace/cbase/vishraylib_properly_structured/trunk/vishcode/cs5721Lib/cs5721GraphicsLib
# Build directory: /home/vishu/workspace/cbase/vishraylib_properly_structured/trunk/vishcode/cs5721Lib/cs5721GraphicsLib/build
# 
# This file includes the relevent testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(src)
SUBDIRS(tests)
SUBDIRS(OpenGL)
SUBDIRS(utests)
