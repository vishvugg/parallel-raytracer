//contains utility functions
#ifndef VISH_RAY_LIB_UTILS
#define VISH_RAY_LIB_UTILS 1
#include "Vector3D.h"
#include <ctime>
using namespace sivelab;
using namespace std;
bool calcQuadraticRoots(const Vector3D&, Vec2D&);
float calcDescriminant(const Vector3D&);
float calcEpsilon();
double max(double i, double j);
double getElapsedSeconds(clock_t& start, int units = 1);
#endif
