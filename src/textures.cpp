#include "textures.h"
using namespace vishraylib;

Vector3D ImageTexture::getPixel(size_t x, size_t y) {

	size_t height = imData->get_height();
	png::rgb_pixel pixel = imData->get_pixel(x, height - y - 1);
	Vector3D colors(pixel.red / 255.0, pixel.green / 255.0, pixel.blue / 255.0);
	//std::cout<<"x "<<x<<" y "<<height-y-1<<" color "<<colors<<std::endl<<std::endl;
	return colors;
}

Vector3D StaticTexture::textureLookup(HitStruct& hitStruct, Vector3D& poi) {
	return staticCoefficient;
}

Vector3D ImageTexture::textureLookup(HitStruct& hitStruct, Vector3D& poi) {
	Vec2D dummy = hitStruct.shape->getS_And_T(poi);
	size_t i = floor(imData->get_width() * dummy.x);
	size_t j = floor(imData->get_height() * dummy.y);
	Vector3D colors = getPixel(i, j);
	return colors;
}

Vector3D ImageTexture::textureLookup(Vec2D& dummy) {
	size_t i = floor(imData->get_width() * dummy.x);
	size_t j = floor(imData->get_height() * dummy.y);
	Vector3D colors = getPixel(i, j);
	return colors;
}

Vector3D PerlinNoise::textureLookup(HitStruct& hitStruct, Vector3D& poi) {
	double k1 = 1;
	double k2 = 1;
	double w = 0.001;
	Vector3D temp = k2 * poi;
	//
	//need to implement striping here
	//
	//double t = (1 + sin(k1 * poi[2] + turbulenece(temp))/w)/2.0;
	//return t * Vector3D(0.2, 0.2, 0.2) + (1 - t) * Vector3D(0.2, 0.2, 0.2);
	double noise = turbulenece(poi);
	return Vector3D(noise, noise, noise);
}

double PerlinNoise::generateNoise(Vector3D &poi) {
	int f_x = floor(poi[0]);
	int f_y = floor(poi[1]);
	int f_z = floor(poi[2]);
	double noise = 0;
	for (int i = f_x; i < f_x + 2; i++)
		for (int j = f_y; j < f_y + 2; j++)
			for (int k = f_z; k < f_z + 2; k++) {
				noise += omega(i, j, k, poi[0] - i, poi[1] - j, poi[2] - k);
			}
	noise = (noise + 1) / 2;
	return noise;
}

double PerlinNoise::turbulenece(Vector3D& poi) {
	double noise = 0;
	for (int i = 0; i < 16; i++) {
		Vector3D tempVector = poi * pow(2, i);
		double temp = generateNoise(tempVector);
		temp /= pow(2, i);
		noise += temp;
	}
	return noise;
}
