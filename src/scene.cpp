#include "scene.h"
#include "hitStruct.h"
#include "utils.h"

using namespace vishraylib;
using namespace sivelab;
//very important must declare a static variable defined in as a class member
//std::vector<Camera*> Scene::cameras;
//std::vector<Shape*> Scene::shapes;
//std::vector<Shader*> Scene::shaders;
//std::vector<Light*> Scene::lights;
Scene* Scene::sceneInstance;

/**
 *
 * returns vector of pointers to cameras in the scene
 */
std::vector<Camera*>& Scene::getCameras() {
	return cameras;
}
/**
 *
 * returns vector of pointers to objects in the scene
 */
std::vector<Shape*>& Scene::getShapes() {
	return shapes;
}
/**
 *
 * @param shaderName
 * @return pointer to the shader object
 */
Shader* Scene::getShader(const string& shaderName) {
	return shaders[shaderName];

}

void Scene::putShader(const string& shaderName, Shader* shader) {
	shaders[shaderName] = shader;
}

Shape* Scene::getBaseObject(const string& baseObjectName) {
	return baseObjects[baseObjectName];

}

void Scene::putBaseObject(const string& baseObjectName, Shape* shape) {
	baseObjects[baseObjectName] = shape;
}

map<string, Shape*> Scene::getBaseObjectMap() {
	return baseObjects;
}

Texture* Scene::getTexture(const string& textureName) {
	return textures[textureName];
}

void Scene::putTexture(const string& textureName, Texture* texture) {

	textures[textureName] = texture;
}
/**
 *
 * returns vector of pointers to lights in the scene
 */
std::vector<Light*>& Scene::getLights() {
	return lights;
}

BvhNode* Scene::getRoot() {
	return root;
}

void Scene::setRoot(BvhNode* p_root) {
	root = p_root;
}

void Scene::print() {
	cout << "No. of cameras read..." << cameras.size() << endl;
	cout << "No. of shapes read..." << shapes.size() << endl;
	cout << "No. of shaders read..." << shaders.size() << endl;
	cout << "No. of textures read..." << textures.size() << endl;
	cout << "No. of lights read..." << lights.size() << endl;
	cout << "No. of base objects read..." << baseObjects.size() << endl;
}

/**
 * Compute color for a given ray
 * @param ray
 * @param tmin
 * @param tmax
 * @param depth
 * @return color of the point on an object this ray hits
 */
Vector3D Scene::computeRayColor(Ray& ray, double tmin, double tmax, int depth,
		Vec2D& s) {
	Scene *scene = Scene::getInstance();
	BvhNode* root = scene->getRoot();
	if (depth <= 0) {
		return getBackground(ray);
	}

	std::vector<Shape*> shapes = scene->getShapes();
	//find the closest object for this Ray
	HitStruct hitStruct;
	if (args.splitMethod != "split") {
		for (int j = 0; j < shapes.size(); j++) {
			//std::cout<<"Trying shape name: "<<shapes[j]->getName()<<std::endl;
			shapes[j]->intersect(ray, tmin, tmax, hitStruct);
		}
	} else {
		int oldLeafHits = leafHits;
		int oldboxHits = bboxHits;
		root->intersect(ray, tmin, tmax, hitStruct);
	}
	if (hitStruct.shape != NULL) {
		string shaderName = hitStruct.shaderName;
		Shader* shader = hitStruct.shader;
		//std::cout<<"shape name: "<<hitStruct.shape->getName()<<"with tmax: "<<tmax;
		//std::cout<<"shader name: "<<shaderName<<std::endl;
		Vector3D color = shader->applyShade(ray, tmin, tmax, depth, hitStruct,
				s);
		//cout<<color<<endl;
		return color;
		//hitStruct.shape->getShaderName()->applyShader(hitStruct);
	}
	//return background color
	//std::cout << "ray did not hit any object" << std::endl;
	return getBackground(ray);
}
/**
 * Checks whether there is any shape in the path of the shadow
 * @param shadowRay
 */
unsigned Scene::isNotInShadow(const Ray& shadowRay) {
	//remove magic value

	double shadow_t_min = 1.0e-3;
	double shadow_t_max = 1;
	HitStruct hitInfo;
	bool hit;
	if (args.splitMethod == "split") {
		BvhNode* root = getRoot();
		hit = root->shadowRayIntersect(shadowRay, shadow_t_min, shadow_t_max, hitInfo);
		if (hitInfo.shape != NULL) {
			return 0;
		}
	} else {
		for (int k = 0; k < shapes.size(); k++) {
			double shadow_t_max = 1;
			//dummy object
			HitStruct hitInfo;
			hit = shapes[k]->intersect(shadowRay, shadow_t_min, shadow_t_max,
					hitInfo);
			if (hitInfo.shape != NULL) {
				return 0;
			}
		}
	}
	return 1;
}

/**
 * Computes the background color for a ray which has not hit any shape
 * @param ray
 * @return backgroundcolor
 */
Vector3D Scene::getBackground(Ray &ray) {
	if (!(getTexture("background") || getTexture("background_posX")))
		return Vector3D(0.2, 0.2, 0.2);

	Vector3D direction = ray.getDirection();
	double x = direction[0];
	double y = direction[1];
	double z = direction[2];
	string cubemap = "background";
	Vec2D s_and_t;
	Vector3D bgColor;
	if (getTexture("background")) {
		//sending a dummy hit structure and a dummy point of intersection
		HitStruct hitStruct;
		Vector3D poi;
		bgColor = getTexture("background")->textureLookup(hitStruct, poi);
	} else {

		struct {
			Vec2D getS_And_T(double max, double min1, double min2) {
				double u = ((min2 / fabs(max)) + 1) / 2;
				double v = ((min1 / fabs(max)) + 1) / 2;
				Vec2D s_and_t;
				s_and_t.x = u;
				s_and_t.y = v;
				return s_and_t;
			}
		} local;

		string cubemap;
		if (fabs(x) > fabs(y) && fabs(x) > fabs(z)) {
			if (x > 0) {
				cubemap = "posX";
				s_and_t = local.getS_And_T(x, y, -z);
			} else {
				cubemap = "negX";
				s_and_t = local.getS_And_T(x, y, -z);
			}
		} else if (fabs(y) > fabs(z)) {
			if (y > 0) {
				cubemap = "posY";
				s_and_t = local.getS_And_T(y, -z, x);
			} else {
				cubemap = "negY";
				s_and_t = local.getS_And_T(y, z, -x);
			}
		} else {
			if (z > 0) {
				cubemap = "posZ";
				s_and_t = local.getS_And_T(z, y, -x);
			} else {
				cubemap = "negZ";
				s_and_t = local.getS_And_T(z, y, -x);
				//std::cout << direction << std::endl;
				//std::cout << "s: " << s_and_t.x << " t " << s_and_t.y
				//		<< std::endl;
			}
		}
		//std::cout<<cubemap<<"\t"<<direction<<std::endl;

		//another hack for perlin noise
		bgColor =
				((ImageTexture*) getTexture("background_" + cubemap))->textureLookup(
						s_and_t);
		/*if(cubemap=="posZ"){
		 //bgColor.set(1,0,0);
		 }
		 if(cubemap=="negX"){
		 //bgColor.set(0,1,0);
		 }
		 if(cubemap=="posY"){
		 //bgColor.set(0,0,1);
		 }
		 if(cubemap=="posX"){
		 //bgColor.set(0,0,0);
		 }
		 if(cubemap=="negY"){
		 //bgColor.set(0,1,1);
		 }
		 if(cubemap=="negZ"){
		 //bgColor.set(1,1,0);
		 }*/
	}
	return bgColor;
}

