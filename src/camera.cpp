/**
 * Camera class methods
 */
#include "camera.h"
using namespace vishraylib;

float Camera::getFocalLength() const {
	return focalLength;
}

void Camera::setFocalLength(float focalLength) {
	this->focalLength = focalLength;
}

float Camera::getImagePlaneWidth() const {
	return imagePlaneWidth;
}

void Camera::setImagePlaneWidth(float imagePlaneWidth) {
	this->imagePlaneWidth = imagePlaneWidth;
}

const Vector3D& Camera::getLookatPoint() const {
	return lookatPoint;
}

void Camera::setLookatPoint(const Vector3D& lookatPoint) {
	this->lookatPoint = lookatPoint;
}

const string& Camera::getName() const {
	return name;
}

void Camera::setName(const string& name) {
	this->name = name;
}

const Vector3D& Camera::getPosition() const {
	return position;
}

void Camera::setPosition(const Vector3D& position) {
	this->position = position;
}

void Camera::setType(CameraType type) {
	this->type = type;
}

const Vector3D& Camera::getViewDir() const {
	return viewDir;
}

void Camera::setViewDir(const Vector3D& viewDir) {
	this->viewDir = viewDir;
}

//namespace vishraylib{
//declaring a namespace solved the problem:/
//	ostream& operator<<(ostream& out, const Camera& camera){
//			//getting wierd errors when defined in corresponding cpp file
//			out << "name: " << camera.name << std::endl 
//					<< "type: " << camera.typeOfCamera()<< std::endl
//					<< "position: " << camera.position << std::endl
//					<< "viewDir: " << camera.viewDir << std::endl 
//					<< "lookatPoint: " << camera.lookatPoint << std::endl
//					<< "imagePlaneWidth: " << camera.imagePlaneWidth << std::endl;
//		}
//}

string Camera::typeOfCamera() const {
	string types[] = { "perspective", "orthographic" };
	return types[type];
}

/**
 *
 * @param i - pixel row number in the raster image
 * @return pixel row position on image plane
 */
float Camera::calcUCoefficient(double i) {
	return l + ((r - l) * (i + 0.5)) / float(args.width);
}
/**
 *
 * @param j - pixel column number in the raster image
 * @return pixel column position on image plane
 */
float Camera::calcVCoefficient(double j) {
	return b + ((t - b) * (j + 0.5)) / float(args.height);
}
/**
 * Generate a ray going through pixel with position  i,j in image plane for Perspective Camera
 * @param ray
 * @param i pixel row position on image plane
 * @param j pixel column position on image plane
 */

void PerspectiveCamera::computeRay(Ray& ray, double i, double j) {
	ray.setDirection(
			-1 * focalLength * camUVW->getW()
					+ camUVW->getU() * calcUCoefficient(i)
					+ camUVW->getV() * calcVCoefficient(j));
	ray.setOrigin(position);
}
/**
 * Generate a ray going through pixel with position  i,j in image plane for Perspective Camera
 * @param ray
 * @param i pixel row position on image plane
 * @param j pixel column position on image plane
 */
void OrthographicCamera::computeRay(Ray& ray, double i, double j) {
	ray.setOrigin(
			position + camUVW->getU() * calcUCoefficient(i)
					+ camUVW->getV() * calcVCoefficient(j));

	ray.setDirection(-1 * camUVW->getW());
}
