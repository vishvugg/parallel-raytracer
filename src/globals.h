#ifndef VISH_RAY_LIB_GLOBAL
#define VISH_RAY_LIB_GLOBAL 1
#include "handleGraphicsArgs.h"
#include <boost/math/constants/constants.hpp>
#include <limits>
//#include <vector>

extern sivelab::GraphicsArgs args;
//used for calculating no of intersections of bounding boxes
extern int bboxHits;
//used for calculating no of intersections of leaves 
extern int leafHits;
#define T_MIN_SHADOW_RAY 1.0e-3d
#define T_MAX_SHADOW_RAY 1.0d
const double NEG_INFINITY= std::numeric_limits<double>::max() * -1;
const float PI = boost::math::constants::pi<float>();
//used in perlin noise
const int NO_OF_UNIT_RANDOM_VECTORS = 256;
//extern unsigned size;
#endif
