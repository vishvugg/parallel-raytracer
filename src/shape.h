#ifndef VISH_RAY_LIB_SHAPE
#define VISH_RAY_LIB_SHAPE 1
#include <string>
#include <cstdlib>
#include <cmath> 
#include <string>

#include "Vector3D.h"
#include "globals.h"
#include "ray.h"
#include "light.h"
#include "scene.h"
#include "shader.h"
#include "hitStruct.h"
#include "matrix.h"
#include "globals.h"

using namespace std;
using namespace sivelab;
namespace vishraylib {
class HitStruct;
class Shader;

/**
 * class for bounding box operations
 */
class BoundingBox {

public:
	BoundingBox() {
		double negInfinity = NEG_INFINITY;//NEGATIVE INFINITY != numeric_limits<double>::min()
		double posInfinity = numeric_limits<double>::max();
		minPt.set(posInfinity, posInfinity, posInfinity);
		maxPt.set(negInfinity, negInfinity, negInfinity);
	}
	BoundingBox(Vector3D& p_minPt, Vector3D& p_maxPt) :
			minPt(p_minPt), maxPt(p_maxPt) {
	}

	static BoundingBox combine(const BoundingBox& bbox1,
			const BoundingBox& bbox2) {
		Vector3D bb_minExtent = minExtent(bbox1.minPt, bbox2.minPt);
		Vector3D bb_maxExtent = maxExtent(bbox1.maxPt, bbox2.maxPt);
		return BoundingBox(bb_minExtent, bb_maxExtent);
	}

	Vector3D getMinPt() {
		return minPt;
	}
	Vector3D getMaxPt() {
		return maxPt;
	}
	void setMinPt(const Vector3D& minPt) {
		this->minPt = minPt;
	}
	void setMaxPt(const Vector3D& maxPt) {
		this->maxPt = maxPt;
	}
	inline double getXCenter() {
		return (minPt[0] + maxPt[0]) / 2;
	}
	inline double getYCenter() {
		return (minPt[1] + maxPt[1]) / 2;
	}
	inline double getZCenter() {
		return (minPt[2] + maxPt[2]) / 2;
	}
	inline Vector3D getCenter() {
		return Vector3D(getXCenter(), getYCenter(), getZCenter());
	}
	static Vector3D minExtent(const Vector3D &pt_1, const Vector3D &pt_2) {
		return Vector3D(min(pt_1[0], pt_2[0]), min(pt_1[1], pt_2[1]),
				min(pt_1[2], pt_2[2]));
	}
	static Vector3D maxExtent(const Vector3D &pt_1, const Vector3D &pt_2) {
		return Vector3D(max(pt_1[0], pt_2[0]), max(pt_1[1], pt_2[1]),
				max(pt_1[2], pt_2[2]));
	}

	inline bool intersect(const Ray& ray) {
		bboxHits++;
		Vector3D direction = ray.getDirection();
		Vector3D origin = ray.getOrigin();
		double a = 1 / direction[0];
		double b = 1 / direction[1];
		double c = 1 / direction[2];
		double tx_min, tx_max, ty_min, ty_max, tz_min, tz_max;
		double xmin = minPt[0];
		double xmax = maxPt[0];

		double ymin = minPt[1];
		double ymax = maxPt[1];

		double zmin = minPt[2];
		double zmax = maxPt[2];

		if (a >= 0) {
			tx_min = a * (xmin - origin[0]);
			tx_max = a * (xmax - origin[0]);
		} else {
			tx_min = a * (xmax - origin[0]);
			tx_max = a * (xmin - origin[0]);
		}

		if (b >= 0) {
			ty_min = b * (ymin - origin[1]);
			ty_max = b * (ymax - origin[1]);
		} else {
			ty_min = b * (ymax - origin[1]);
			ty_max = b * (ymin - origin[1]);
		}

		if (c >= 0) {
			tz_min = c * (zmin - origin[2]);
			tz_max = c * (zmax - origin[2]);
		} else {
			tz_min = c * (zmax - origin[2]);
			tz_max = c * (zmin - origin[2]);
		}

		if ((tx_min > ty_max || ty_min > tx_max)
				|| (tx_min > tz_max || tz_min > tx_max)
				|| (ty_min > tz_max || tz_min > ty_max)) {
			return false;
		}

		return true;
	}

	void print() {
		cout << "min point" << minPt << endl;
		cout << "max point" << maxPt << endl;
	}
private:
	Vector3D minPt;
	Vector3D maxPt;

};

/**
 *
 * Abstract shape class
 */
class Shape {

public:

	Shape() {
	}
	virtual ~Shape() {
	}
	virtual void print()=0;
	virtual bool intersect(const Ray& r, const double tmin, double& tmax,
			HitStruct& hitStruct)=0;
	//removed virtual as it is no longer used as shape::getNormal() in shaders
	//it is used internally in each shape class and the normal is stored in hitStruct
	void getNormal(Vector3D& poi, Vector3D& normal);
	//to-do put s and t in hitStruct iteslf removing virtual here
	virtual Vec2D getS_And_T(Vector3D& poi)=0;
	//virtual void makeBoundingBox()=0
	Vector3D color;
	string getShaderName() {
		return shaderName;
	}
	string getName() {
		return name;
	}
	void putName(string& name) {
		this->name = name;
	}
	void putShaderName(string& shaderName) {
		this->shaderName = shaderName;
	}
	inline BoundingBox getBoundingBox() {
		return bbox;
	}
	void setBoundingBox(BoundingBox& bbox) {
		this->bbox = bbox;
	}
	virtual bool shadowRayIntersect(const Ray& r, const double tmin, double& tmax,
			HitStruct& hitStruct){
			intersect(r, tmin, tmax, hitStruct);
	}
protected:
	Shape(const string& p_name, const string& p_shaderName) :
		name(p_name), shaderName(p_shaderName) {

	}
	string shaderName;
	string name;
public:
	BoundingBox bbox;
};

/**
 * class for BVH node
 */
class BvhNode: public Shape {
public:

	BvhNode() {
		left = NULL;
		right = NULL;
		//lnode=NULL;
		//rnode=NULL;
	}
	virtual ~BvhNode() {
	}
	void print() {
	}
	bool intersect(const Ray& r, const double tmin, double& tmax,
			HitStruct& hitStruct);
	bool shadowRayIntersect(const Ray& r, const double tmin, double& tmax, HitStruct& hitStruct);
	//dummy methods never called since we return actual leaf that is
	//intersected if an intersection occurs not a bvhNode
	Vec2D getS_And_T(Vector3D& poi) {
	}
	BvhNode(vector<Shape*>& shapes, int axis, int level);
	BoundingBox getBoundingBox() {
		return bbox;
	}

	Shape* left;
	Shape* right;
	//BvhNode* lnode;
	//BvhNode* rnode;
	int level;
private:
};

inline bool BvhNode::intersect(const Ray& ray, const double tmin, double& tmax,
		HitStruct& hitStruct) {
	if (bbox.intersect(ray)) {
		bool leftHit = (left != NULL)
				&& left->intersect(ray, tmin, tmax, hitStruct);
		bool rightHit = (right != NULL)
				&& right->intersect(ray, tmin, tmax, hitStruct);

		if (leftHit || rightHit) {
			return true;
		} else
			return false;
	}
	return false;
}
/* 
 * Instanced Object
 */

class InstanceObject: public Shape {

public:
	InstanceObject(const string& p_name, const string& p_shaderName,
			Shape *p_baseObjPtr, Matrix& p_inverseTransformationMatrix) :
			Shape(p_name, p_shaderName), baseObjPtr(p_baseObjPtr), inverseTransformationMatrix(
					p_inverseTransformationMatrix) {

		bbox = baseObjPtr->getBoundingBox();
		Vector3D minPt = bbox.getMinPt();
		Vector3D maxPt = bbox.getMaxPt();
		//calculating base object bounding box vertices
		vector<Vector3D> vertices;
		vertices.push_back(minPt);
		vertices.push_back(Vector3D(maxPt[0], minPt[1], minPt[2]));
		vertices.push_back(Vector3D(maxPt[0], minPt[1], maxPt[2]));
		vertices.push_back(Vector3D(minPt[0], minPt[1], maxPt[2]));
		vertices.push_back(Vector3D(minPt[0], maxPt[1], minPt[2]));
		vertices.push_back(Vector3D(maxPt[0], maxPt[1], minPt[2]));
		vertices.push_back(maxPt);
		vertices.push_back(Vector3D(minPt[0], maxPt[1], maxPt[2]));
		//transforming bounding box with transformation matrix
		Matrix transformationMatrix = inverseTransformationMatrix.inverse();
		double negInfinity = numeric_limits<double>::min();
		double posInfinity = numeric_limits<double>::max();
		minPt.set(posInfinity, posInfinity, posInfinity);
		maxPt.set(negInfinity, negInfinity, negInfinity);

		for (int i = 0; i < vertices.size(); i++) {
			vertices[i] = transformationMatrix.multiply(vertices[i], 1);
			minPt = BoundingBox::minExtent(minPt, vertices[i]);
			maxPt = BoundingBox::maxExtent(maxPt, vertices[i]);
		}

		bbox.setMinPt(minPt);
		bbox.setMaxPt(maxPt);
	}

	bool intersect(const Ray& r, const double tmin, double& tmax,
			HitStruct& hitStruct);
	void getNormal(Vector3D& poi, Vector3D& normal);
	void print();
	Vec2D getS_And_T(Vector3D& poi);

private:
	Shape* baseObjPtr;
	Matrix inverseTransformationMatrix;

};

/**
 * Sphere class
 */

class Sphere: public Shape {

public:
	Sphere() {
	}
	virtual ~Sphere() {
	}
	Sphere(const string& p_name, Vector3D& p_center, float p_radius,
			const string& p_shaderName) :
			Shape(p_name, p_shaderName), center(p_center), radius(p_radius) {
		makeBoundingBox();
	}
	void print();
	bool intersect(const Ray& r, const double tmin, double& tmax,
			HitStruct& hitStruct);
	void getNormal(Vector3D& poi, Vector3D& normal) {
		normal = poi - center;
		normal.normalize();
	}
	Vec2D getS_And_T(Vector3D& poi);
	//create bounding box for sphere
	void makeBoundingBox() {
		Vector3D radiusVector(radius, radius, radius);
		Vector3D minExtent = (center - radiusVector);
		Vector3D maxExtent = (center + radiusVector);
		bbox.setMinPt(minExtent);
		bbox.setMaxPt(maxExtent);
	}
private:
	Vector3D center;
	float radius;
};

/**
 * Vertex of a Triangle
 */
class Vertex {
public:
	Vertex() {
	}
	Vertex(Vec2D& p_s_and_t, Vector3D& p_coordinates) :
			s_and_t(p_s_and_t), coordinates(p_coordinates) {

	}
	Vertex(Vector3D& p_coordinates, Vector3D& p_normal) :
			coordinates(p_coordinates), normal(p_normal) {

	}
	Vec2D getS_And_T() {
		return s_and_t;
	}
	void setS_And_T(Vec2D& s_and_t) {
		this->s_and_t = s_and_t;
	}
	inline Vector3D getCoordinates() {
		return coordinates;
	}
	void setCoordinates(const Vector3D& coordinates) {
		this->coordinates = coordinates;
	}
	Vector3D getNormal() {
		return normal;
	}
	void setNormal(const Vector3D& normal) {
		this->normal = normal;
	}
private:
	Vector3D coordinates;
	Vector3D normal;
	Vec2D s_and_t;
};

/**
 * Triangle class
 */
class Triangle: public Shape {
public:
	Triangle() {
	}
	virtual ~Triangle() {
	}
	//void* operator new(size_t){
			

	//}
	Triangle(const string& p_name, Vector3D& p_v0, Vector3D& p_v1,
			Vector3D& p_v2, const string& p_shaderName, bool p_normalsExist =
					false) :
			Shape(p_name, p_shaderName), normalsExist(p_normalsExist) {
		v0.setCoordinates(p_v0);
		v1.setCoordinates(p_v1);
		v2.setCoordinates(p_v2);
		makeBoundingBox();
	}

	Triangle(const string& p_name, Vertex& p_v0, Vertex& p_v1, Vertex& p_v2,
			const string& p_shaderName, bool p_normalsExist = false) :
			Shape(p_name, p_shaderName), v0(p_v0), v1(p_v1), v2(p_v2), normalsExist(
					p_normalsExist) {

		makeBoundingBox();
	}
	void print();
	bool intersect(const Ray& r, const double tmin, double& tmax,
			HitStruct& hitStruct);
	void setHitStruct(Vector3D& poi, HitStruct& hitStruct);
	void getNormal(Vector3D& poi, Vector3D& normal) {
		//normal =  (v1-v0).cross(v0-v2);
	
		Vector3D v0 = this->v0.getCoordinates();
		Vector3D v1 = this->v1.getCoordinates();
		Vector3D v2 = this->v2.getCoordinates();
		if (normalsExist) {
			//normal interpolation
			double areaofA_b = (v0 - v2).cross(poi - v2).length();
			double areaofA_c = (v1 - v0).cross(poi - v0).length();
			double areaofA = (v1 - v0).cross(v2 - v0).length();
			double beta = areaofA_b / areaofA;
			double gamma = areaofA_c / areaofA;
			double alpha = 1 - beta - gamma;
			normal = this->v0.getNormal() * alpha + this->v1.getNormal() * beta
					+ this->v2.getNormal() * gamma;
			//cout<<normal<<","<<(v1 - v0).cross(v2-v0)<<endl;
		} else {
			normal = (v1 - v0).cross(v2 - v0);
		}
		normal.normalize();
	
	}
	Vec2D getS_And_T(Vector3D& poi);

	//create bounding box for triangle
	void makeBoundingBox() {
		double negInfinity = numeric_limits<double>::min();
		double posInfinity = numeric_limits<double>::max();

		Vector3D minPt(posInfinity, posInfinity, posInfinity);
		Vector3D maxPt(negInfinity, negInfinity, negInfinity);

		Vector3D v0_coordinates = v0.getCoordinates();
		Vector3D v1_coordinates = v1.getCoordinates();
		Vector3D v2_coordinates = v2.getCoordinates();

		Vector3D bb_minExtent;
		bb_minExtent = BoundingBox::minExtent(v0_coordinates, v1_coordinates);
		bb_minExtent = BoundingBox::minExtent(bb_minExtent, v2_coordinates);

		Vector3D bb_maxExtent;
		bb_maxExtent = BoundingBox::maxExtent(v0_coordinates, v1_coordinates);
		bb_maxExtent = BoundingBox::maxExtent(bb_maxExtent, v2_coordinates);

		bbox.setMinPt(bb_minExtent);
		bbox.setMaxPt(bb_maxExtent);
	}
private:
	Vertex v0, v1, v2;
	bool normalsExist;
};

/**
 *
 * @param ray reference to the ray with which objects are intersecting
 * @param tmin image plane position, defaults to 1
 * @param tmax distance from the image plane to the nearest object so far
 * @param hitStruct structure containing information of the nearest object so far
 * @return boolean indicating whether the ray hits the triangle
 */
inline bool Triangle::intersect(const Ray& ray, const double tmin, double& tmax,
		HitStruct& hitStruct) {
	leafHits++;
	double a, b, c, d, e, f, g, h, i, j, k, l;
	Vector3D v0 = this->v0.getCoordinates();
	Vector3D v1 = this->v1.getCoordinates();
	Vector3D v2 = this->v2.getCoordinates();
	a = v0[0] - v1[0];
	b = v0[1] - v1[1];
	c = v0[2] - v1[2];
	d = v0[0] - v2[0];
	e = v0[1] - v2[1];
	f = v0[2] - v2[2];
	Vector3D rayDir = ray.getDirection();
	Vector3D rayOrigin = ray.getOrigin();
	g = rayDir[0];
	h = rayDir[1];
	i = rayDir[2];
	j = v0[0] - rayOrigin[0];
	k = v0[1] - rayOrigin[1];
	l = v0[2] - rayOrigin[2];
	double ei_minus_hf, gf_minus_di, dh_minus_eg, ak_minus_jb, jc_minus_al,
			bl_minus_kc;
	ei_minus_hf = e * i - h * f;
	gf_minus_di = g * f - d * i;
	dh_minus_eg = d * h - e * g;
	ak_minus_jb = a * k - j * b;
	jc_minus_al = j * c - a * l;
	bl_minus_kc = b * l - k * c;
	double M = a * ei_minus_hf + b * gf_minus_di + c * dh_minus_eg;
	double beta = (j * ei_minus_hf + k * gf_minus_di + l * dh_minus_eg) / M;
	double gamma = (i * ak_minus_jb + h * jc_minus_al + g * bl_minus_kc) / M;
	double t = (f * ak_minus_jb + e * jc_minus_al + d * bl_minus_kc) / -M;
	//	std::cout << "rayOrigin:" << rayOrigin << "\t" << "rayDirection:"
	//			<< rayDir << "\t" << " M: " << M << " t: " << t << " gamma: "
	//			<< gamma << " beta: " << beta << std::endl;

	//std::cout << a << "\t" << d << "\t" << g << "\t" << std::endl << b
	//			<< "\t" << e << "\t" << h << "\t" << std::endl << c << "\t" << f
	//			<< "\t" << i << "\t" << std::endl << j << "\t" << k << "\t" << l
	//			<< "\t" << std::endl << std::endl;

	if (t < tmin || t > tmax)
		return false;
	if (gamma < 0 || gamma > 1)
		return false;
	if (beta < 0 || beta > (1 - gamma))
		return false;
	tmax = t;
	Vector3D poi = ray.getOrigin() + tmax * ray.getDirection();
	setHitStruct(poi, hitStruct);
	return true;
}

class memory{
public:	
	memory(size_t n){
			saddress = malloc( n * sizeof(Triangle));
			remaningTriangles = n;
	}
	void *insertTriangle(){

	}
	void *saddress;
	unsigned remaningTriangles;
};

/**
 * Box class
 */
class Box: public Shape {
public:
	Box() {
	}
	virtual ~Box() {
	}
	Box(const string& p_name, const Vector3D& p_minPt, const Vector3D& p_maxPt,
			const string& p_shaderName) :
			Shape(p_name, p_shaderName), minPt(p_minPt), maxPt(p_maxPt) {
		/*
		 e .+------+ f
		 .' |    .'|
		 h +---+--+'ma|xPt
		 |  |   |   |
		 min|Pt,+--+---+
		 |.'    | .' b
		 +------+'
		 d 		   c

		 the figure below is wrong maxPt should be towards you remember +z axis is towards you
		 h .+------+ maxPt
		 .'  |    .'|
		 e +---+--+'f |
		 |   |  |   |
		 | d,+--+---+
		 |.'    | .' c
		 +------+'
		 minPt		b
		 right hand rule winding
		 */
		Vec2D _00;
		_00.x = 0;
		_00.y = 0;

		Vec2D _01;
		_01.x = 0;
		_01.y = 1;

		Vec2D _10;
		_10.x = 1;
		_10.y = 0;

		Vec2D _11;
		_11.x = 1;
		_11.y = 1;

		const Vector3D &coordinate_a = p_minPt;
		Vector3D coordinate_b = Vector3D(p_maxPt[0], p_minPt[1], p_minPt[2]);
		Vector3D coordinate_c = Vector3D(p_maxPt[0], p_minPt[1], p_maxPt[2]);
		Vector3D coordinate_d = Vector3D(p_minPt[0], p_minPt[1], p_maxPt[2]);
		Vector3D coordinate_e = Vector3D(p_minPt[0], p_maxPt[1], p_minPt[2]);
		Vector3D coordinate_f = Vector3D(p_maxPt[0], p_maxPt[1], p_minPt[2]);
		const Vector3D &coordinate_g = p_maxPt;
		Vector3D coordinate_h = Vector3D(p_minPt[0], p_maxPt[1], p_maxPt[2]);
		//std::cout<<"a = "<<p_minPt<<"b = "<<b<<"c = "<<c<<"d = "<<d<<"e = "<<e<<"f = "<<f<<"g = "<<p_maxPt<<"h = "<<h<<std::endl;

		Vertex a, b, c, d, e, f, g, h;

		//floor triangles
		d.setCoordinates(coordinate_d);
		d.setS_And_T(_01);
		c.setCoordinates(coordinate_c);
		c.setS_And_T(_11);
		a.setCoordinates(coordinate_a);
		a.setS_And_T(_00);
		b.setCoordinates(coordinate_b);
		b.setS_And_T(_10);

		triangles.push_back(
				new Triangle(p_name + "_triangle_floor_1", d, a, b,
						p_shaderName));

		triangles.push_back(
				new Triangle(p_name + ("_triangle_floor_2"), d, b, c,
						p_shaderName));

		//right side triangles
		b.setCoordinates(coordinate_b);
		b.setS_And_T(_10);
		c.setCoordinates(coordinate_c);
		c.setS_And_T(_00);
		f.setCoordinates(coordinate_f);
		f.setS_And_T(_11);
		g.setCoordinates(coordinate_g);
		g.setS_And_T(_01);

		triangles.push_back(
				new Triangle(p_name + ("_triangle_right_1"), f, c, b,
						p_shaderName));

		triangles.push_back(
				new Triangle(p_name + ("_triangle_right_2"), f, g, c,
						p_shaderName));

		//left side triangles
		d.setCoordinates(coordinate_d);
		d.setS_And_T(_10);
		a.setCoordinates(coordinate_a);
		a.setS_And_T(_00);
		h.setCoordinates(coordinate_h);
		h.setS_And_T(_11);
		e.setCoordinates(coordinate_e);
		e.setS_And_T(_01);

		triangles.push_back(
				new Triangle(p_name + ("_triangle_left_1"), e, a, h,
						p_shaderName));

		triangles.push_back(
				new Triangle(p_name + ("_triangle_left_2"), a, d, h,
						p_shaderName));

		//back side triangles
		a.setCoordinates(coordinate_a);
		a.setS_And_T(_10);
		b.setCoordinates(coordinate_b);
		b.setS_And_T(_00);
		e.setCoordinates(coordinate_e);
		e.setS_And_T(_11);
		f.setCoordinates(coordinate_f);
		f.setS_And_T(_01);

		triangles.push_back(
				new Triangle(p_name + ("_triangle_back_1"), b, a, e,
						p_shaderName));

		triangles.push_back(
				new Triangle(p_name + ("_triangle_back_2"), b, e, f,
						p_shaderName));

		//front side triangles
		c.setCoordinates(coordinate_c);
		c.setS_And_T(_10);
		d.setCoordinates(coordinate_d);
		d.setS_And_T(_00);
		g.setCoordinates(coordinate_g);
		g.setS_And_T(_11);
		h.setCoordinates(coordinate_h);
		h.setS_And_T(_01);
		triangles.push_back(
				new Triangle(p_name + ("_triangle_front_1"), h, c, g,
						p_shaderName));

		triangles.push_back(
				new Triangle(p_name + ("_triangle_front_2"), h, d, c,
						p_shaderName));

		//top side triangles
		e.setCoordinates(coordinate_e);
		e.setS_And_T(_01);
		f.setCoordinates(coordinate_f);
		f.setS_And_T(_11);
		h.setCoordinates(coordinate_h);
		h.setS_And_T(_00);
		g.setCoordinates(coordinate_g);
		g.setS_And_T(_10);

		triangles.push_back(
				new Triangle(p_name + ("_triangle_top_1"), e, h, f,
						p_shaderName));

		triangles.push_back(
				new Triangle(p_name + ("_triangle_top_2"), g, f, h,
						p_shaderName));

		makeBoundingBox();
		//print();
		//bbox.print();
	}
	void print();
	bool intersect(const Ray& r, const double tmin, double& tmax,
			HitStruct& hitStruct);
	void getNormal(Vector3D& poi, Vector3D& normal) {
		std::cout << "I am being called! Seriously?";
	}
	Vec2D getS_And_T(Vector3D& poi);

	//create bounding box for a box
	void makeBoundingBox() {
		bbox.setMinPt(minPt);
		bbox.setMaxPt(maxPt);
	}
private:
	Vector3D minPt, maxPt;
	std::vector<Triangle*> triangles;
};

/**
 * Class for Mesh
 */
class OBJMesh: public Shape {

public:
	OBJMesh() {
	}
	OBJMesh(string& name, string& shaderName, vector<Shape*>& p_triangles,
			BoundingBox& p_bbox) :
			Shape(name, shaderName), triangles(p_triangles) {
		//size += sizeof(Triangle)*triangles.size();
		bbox = p_bbox;
		root = new BvhNode(triangles, 0, 0);
	}
	virtual ~OBJMesh() {
	}
	void print();
	bool intersect(const Ray&r, const double tmin, double& tmax,
			HitStruct& hitStruct);
	void getNormal(Vector3D& poi, Vector3D& normal);
	Vec2D getS_And_T(Vector3D& poi);
	//bounding box is calculated during XMl parsing and shape creating, this never called 
	void makeBoundingBox() {
	}
private:
	vector<Shape*> triangles;
	BvhNode* root;
};

}
#endif
