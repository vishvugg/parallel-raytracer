#include "matrix.h"
#include <vector>
#include <algorithm>
#include "globals.h"

using namespace vishraylib;
using namespace sivelab;
using namespace std;

namespace vishraylib {
ostream &operator<<(ostream& os, const Matrix &m) {
	for (int i = 0; i < m.num_rows; i++) {
		os << '[';
		for (int j = 0; j < m.num_columns; j++)
			os << m.rows[i][j] << ' ';
		os << ']' << std::endl;
	}
	os << std::endl;
	return os;
}
}

Matrix::Matrix(int num_rows, int num_columns, std::vector<double>& elements) {
	assert(elements.size() == num_rows * num_columns);
	int i;
	this->num_rows = num_rows;
	this->num_columns = num_columns;
	int k = 0;
	for (i = 0; i < num_rows; i++) {
		vector<double> row;
		rows.push_back(row);
		for (int j = 0; j < num_columns; j++) {
			rows[i].push_back(elements[k++]);
		}
	}
}

Matrix Matrix::transpose() {
	Matrix m = *this;
	//cout<<m;
	for (int i = 0; i < m.num_rows; i++) {
		for (int j = 0; j < m.num_columns; j++) {
			if (i > j) {
				//cout<<m.rows[i][j]<<","<<m.rows[j][i]<<endl;
				swap(m.rows[i][j], m.rows[j][i]);
			}
		}
	}
	swap(m.num_rows, m.num_columns);
	return m;
}

Matrix Matrix::multiply(const Matrix& m) {
	assert(num_columns == m.num_rows);
	vector<double> elements;
	for (int i = 0; i < num_rows; i++) {
		for (int j = 0; j < m.num_columns; j++) {
			double sum = 0;
			for (int k = 0; k < num_columns; k++) {
				sum += rows[i][k] * m.rows[k][j];
			}
			elements.push_back(sum);
		}
	}
	Matrix result(num_rows, m.num_columns, elements);
	return result;
}
/**
 *
 * @param vec vector to multiply
 * @param vectorType 1 for position and 0 for rays
 * @return
 */
Vector3D Matrix::multiply(const Vector3D& vec, const int vectorType) {
	vector<double> elements;
	elements.push_back(vec[0]);
	elements.push_back(vec[1]);
	elements.push_back(vec[2]);
	elements.push_back(vectorType);
	Matrix vectorMatrix(4, 1, elements);

	Matrix result = multiply(vectorMatrix);
	//do I nedd to delete result explictly?
	return Vector3D(result.rows[0][0], result.rows[1][0], result.rows[2][0]);
}

Matrix Matrix::buildMinor(int row, int column) {
	vector<double> elements;
	for (int i = 0; i < num_rows; i++) {
		for (int j = 0; j < num_columns; j++) {
			if (i != row && j != column)
				elements.push_back(rows[i][j]);
		}
	}
	Matrix temp(num_rows - 1, num_columns - 1, elements);
	return temp;
}

double Matrix::determinant() {
	assert(num_rows == num_columns);
	//compute the det if it is a 2x2 matrix
	if (num_rows == 2) {
		double a = rows[0][0];
		double b = rows[0][1];
		double c = rows[1][0];
		double d = rows[1][1];
		return a * d - b * c;
	}
	double det = 0;
	//just first row is enough for det calculations
	int i = 0;
	for (int j = 0; j < num_columns; j++) {
		//for each element build a minor matrix
		Matrix minorDet = buildMinor(i, j);
		int cofactorSign = pow(-1, i + j);
		det += cofactorSign * rows[0][j] * minorDet.determinant();
	}
	return det;
}

Matrix Matrix::getIdentityMatrix(int size) {
	vector<double> elements(size * size, 0);
	for (int i = 0; i < size; i++) {
		elements[i * (size + 1)] = 1;
	}
	return Matrix(size, size, elements);
}

Matrix Matrix::inverse() {
	vector<double> elements;
	double det = determinant();
	//cout<<det<<endl;
	assert(det != 0);
	for (int i = 0; i < num_rows; i++) {
		for (int j = 0; j < num_columns; j++) {
			//cout<<"building minors for "<<i<<", "<<j<<endl;
			elements.push_back(
					pow(-1, i + j) * buildMinor(i, j).determinant() / det);

		}
	}
	Matrix cofactorsMatrix(num_rows, num_columns, elements);
	//cout<<"cofactors matrix "<<endl<<cofactorsMatrix;
	Matrix inverse = cofactorsMatrix.transpose();
	//cout<<"inverse is "<<endl<<inverse;
	return inverse;
}

#define twoDto1D(x,y,size) x*size+y

Matrix Matrix::getRotateX(float degreesOfRotation) {
	int size = 4;
	vector<double> elements(size * size, 0);
	float theta = degreesOfRotation * PI / 180;
	float sinTheta = sin(theta);
	float cosTheta = cos(theta);
	elements[ twoDto1D(0, 0, size)] = 1;
	elements[ twoDto1D(1, 1, size)] = cosTheta;
	elements[ twoDto1D(1, 2, size)] = -sinTheta;
	elements[ twoDto1D(2, 1, size)] = sinTheta;
	elements[ twoDto1D(2, 2, size)] = cosTheta;
	elements[ twoDto1D(3, 3, size)] = 1;
	return Matrix(4, 4, elements);
}

Matrix Matrix::getRotateY(float degreesOfRotation) {
	int size = 4;
	vector<double> elements(size * size, 0);
	float theta = degreesOfRotation * PI / 180;
	float sinTheta = sin(theta);
	float cosTheta = cos(theta);
	elements[ twoDto1D(0, 0, size)] = cosTheta;
	elements[ twoDto1D(0, 2, size)] = sinTheta;
	elements[ twoDto1D(1, 1, size)] = 1;
	elements[ twoDto1D(2, 0, size)] = -sinTheta;
	elements[ twoDto1D(2, 2, size)] = cosTheta;
	elements[ twoDto1D(3, 3, size)] = 1;
	return Matrix(4, 4, elements);
}

Matrix Matrix::getRotateZ(float degreesOfRotation) {
	int size = 4;
	vector<double> elements(size * size, 0);
	float theta = degreesOfRotation * PI / 180;
	float sinTheta = sin(theta);
	float cosTheta = cos(theta);
	elements[ twoDto1D(0, 0, size)] = cosTheta;
	elements[ twoDto1D(0, 1, size)] = -sinTheta;
	elements[ twoDto1D(1, 0, size)] = sinTheta;
	elements[ twoDto1D(1, 1, size)] = cosTheta;
	elements[ twoDto1D(2, 2, size)] = 1;
	elements[ twoDto1D(3, 3, size)] = 1;
	return Matrix(4, 4, elements);

}

Matrix Matrix::getTranslate(float translateX, float translateY,
		float translateZ) {
	int size = 4;
	vector<double> elements(size * size, 0);
	for (int i = 0; i < size; i++) {
		elements[i * (size + 1)] = 1;
	}
	elements[ twoDto1D(0, 3, size)] = translateX;
	elements[ twoDto1D(1, 3, size)] = translateY;
	elements[ twoDto1D(2, 3, size)] = translateZ;
	return Matrix(4, 4, elements);

}

Matrix Matrix::getScale(float scaleX, float scaleY, float scaleZ) {
	int size = 4;
	vector<double> elements(size * size, 0);
	elements[ twoDto1D(0, 0, size)] = scaleX;
	elements[ twoDto1D(1, 1, size)] = scaleY;
	elements[ twoDto1D(2, 2, size)] = scaleZ;
	elements[ twoDto1D(3, 3, size)] = 1;
	return Matrix(4, 4, elements);
}
