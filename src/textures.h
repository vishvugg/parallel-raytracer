#ifndef VISH_RAY_LIB_TEXTURES
#define VISH_RAY_LIB_TEXTURES 1

#include <algorithm>

#include "png++/png.hpp"
#include "Vector3D.h"
#include "hitStruct.h"

using namespace sivelab;
using namespace std;
namespace vishraylib {

/**
 * Generic texture class
 */
class HitStruct;
class Texture {

public:
	//try passing poi also in hitstructure or
	//just pass the appropriate shape and point of intersection
	virtual Vector3D textureLookup(HitStruct& hitStruct, Vector3D& poi)=0;
	virtual ~Texture() {
	}
};

/**
 *	Static Texture class used for holding static diffuse, specular coefficients
 *	 read from scene file
 */
class StaticTexture: public Texture {
public:
	StaticTexture(Vector3D& p_staticCoefficient) :
			staticCoefficient(p_staticCoefficient) {
		//print();
	}
	void print() {
		cout << "Static coefficient " << staticCoefficient << std::endl;
	}
	Vector3D textureLookup(HitStruct& hitStruct, Vector3D& poi);
private:
	Vector3D staticCoefficient; //can be diffuse or specular
};

/**
 * Image texture used for holding image textures and calculating diffuse,
 * specular coefficients from the images
 */
class ImageTexture: public Texture {

public:
	ImageTexture(png::image<png::rgb_pixel>* p_imData) :
			imData(p_imData) {
	}
	Vector3D getPixel(size_t row, size_t column);
	Vector3D textureLookup(HitStruct& hitStruct, Vector3D& poi);
	Vector3D textureLookup(Vec2D& dummy);
private:
	png::image<png::rgb_pixel>* imData;
};

/**
 * Class for perlin noise
 */
class PerlinNoise: public Texture {

private:
	void generate_N_RandomVectors(int n) {
		for (int i = 0; i < n; i++) {
			Vector3D rand;
			double vx, vy, vz;
			do {

				vx = 2 * drand48() - 1;
				vy = 2 * drand48() - 1;
				vz = 2 * drand48() - 1;

			} while ((vx * vx + vy * vy + vz * vz) < 1);
			rand.set(vx, vy, vz);
			randomUnitVectors.push_back(rand);
		}
	}

	int phi(int i) {
		return rangePermutation[i % NO_OF_UNIT_RANDOM_VECTORS];
	}

	double weightingFunction(double t) {
		t = fabs(t);
		if (t < 1)
			return 2 * t * t * t - 3 * t * t + 1;
		else
			return 0;
	}

	Vector3D tau(int i, int j, int k) {
		return randomUnitVectors[(phi(i + phi(j + phi(k))))
				% NO_OF_UNIT_RANDOM_VECTORS];
	}

	double omega(int i, int j, int k, double u, double v, double w) {
		return weightingFunction(u) * weightingFunction(v)
				* weightingFunction(w) * (tau(i, j, k).dot(Vector3D(u, v, w)));
	}

	double turbulenece(Vector3D& poi);
	double generateNoise(Vector3D &poi);
public:
	PerlinNoise() {
		generate_N_RandomVectors(NO_OF_UNIT_RANDOM_VECTORS);
		for (int i = 0; i < NO_OF_UNIT_RANDOM_VECTORS; i++) {
			rangePermutation.push_back(i);
		}
		random_shuffle(rangePermutation.begin(), rangePermutation.end());
	}
	Vector3D textureLookup(HitStruct& hitStruct, Vector3D& poi);
	vector<Vector3D> randomUnitVectors;
	vector<int> rangePermutation;

};

}
#endif
