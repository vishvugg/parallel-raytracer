#ifndef VISH_RAY_LIB_SHADER
#define VISH_RAY_LIB_SHADER 1
#include <string>

#include "Vector3D.h"
#include "globals.h"
#include "hitStruct.h"
#include "textures.h"

using namespace std;
using namespace sivelab;
namespace vishraylib {
class HitStruct;
/**
 * abstract shader class
 */
class Shader {
public:
	Shader() {
	}
	virtual ~Shader() {
	}
	virtual void print()=0;
	virtual Vector3D applyShade(Ray& ray, double tmin, double tmax, int depth,
			HitStruct& hitStruct, Vec2D& s)=0;
	string* getName() {
		return &name;
	}
protected:
	Shader(const string& p_name) :
			name(p_name) {
	}
	string name;
};
/**
 * Mirror shader class
 */
class Mirror: public Shader {
public:
	Mirror() {
	}
	virtual ~Mirror() {
	}
	Mirror(const string& p_name, double p_roughness) :
			Shader(p_name), roughness(p_roughness) {
		//print();
	}
	void print();
	Vector3D applyShade(Ray& ray, double tmin, double tmax, int depth,
			HitStruct& hitStruct, Vec2D& s);
	double getRoughness() {
		return roughness;
	}
private:
	double roughness;
};

/**
 * Lambertian shader class
 */
class Lambertian: public Shader {
public:
	Lambertian() {
	}
	virtual ~Lambertian() {
	}
	Lambertian(const string& p_name, const string& p_diffuse) :
			Shader(p_name), diffuse(p_diffuse) {
		//print();
	}
	void print();
	Vector3D applyShade(Ray& ray, double tmin, double tmax, int depth,
			HitStruct& hitStruct, Vec2D& s);
private:
	string diffuse;
};
/**
 * Glaze shader class
 */
class Glaze: public Shader {
public:
	Glaze() {
	}
	virtual ~Glaze() {
	}
	Glaze(const string& p_name, string p_diffuse, float p_mirrorCoef,
			double p_roughness) :
			Shader(p_name), diffuse(p_diffuse), mirrorCoef(p_mirrorCoef), roughness(
					p_roughness) {
		//print();
	}
	void print();
	Vector3D applyShade(Ray& ray, double tmin, double tmax, int depth,
			HitStruct& hitStruct, Vec2D& s);

	double getRoughness() {
		return roughness;
	}
private:
	string diffuse;
	float mirrorCoef;
	double roughness;
};
/**
 * Blinnphong shader class
 */
class Blinnphong: public Shader {
public:
	Blinnphong() {
	}
	virtual ~Blinnphong() {
	}
	Blinnphong(const string& p_name, const string& p_diffuse,
			const string& p_specular, float p_phongExp) :
			Shader(p_name), diffuse(p_diffuse), specular(p_specular), phongExp(
					p_phongExp) {
		//print();
	}
	void print();
	Vector3D applyShade(Ray& ray, double tmin, double tmax, int depth,
			HitStruct& hitStruct, Vec2D& s);
private:
	string diffuse;
	string specular;
	float phongExp;
};
/**
 * BlinnPhongMirrored shader
 */
class BlinnPhongMirrored: public Shader {
public:

	BlinnPhongMirrored(const string& p_name, const string& p_diffuse,
			const string& p_specular, float p_phongExp, float p_mirrorCoef,
			double p_roughness) :
			Shader(p_name), diffuse(p_diffuse), specular(p_specular), phongExp(
					p_phongExp), mirrorCoef(p_mirrorCoef), roughness(
					p_roughness) {
		//print();
	}
	void print();
	Vector3D applyShade(Ray& ray, double tmin, double tmax, int depth,
			HitStruct& hitStruct, Vec2D& s);
	double getRoughness() {
		return roughness;
	}

private:
	string diffuse; //when decalread as Vector3D& diffuse ->error: uninitialized reference member ‘vishraylib::Blinnphong::diffuse’ [-fpermissive]
	string specular;
	float phongExp;
	float mirrorCoef;
	double roughness;
};
}
#endif
