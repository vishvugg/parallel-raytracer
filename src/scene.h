//singleton class not thread safe
//holds all cameras, lights, objects, shaders etc
#ifndef VISH_RAY_LIB_SCENE
#define VISH_RAY_LIB_SCENE 1
#include "camera.h"
#include "shader.h"
#include "light.h"
#include "ray.h"
#include "Vector3D.h"
#include "textures.h"
#include "string"
namespace vishraylib {
class Shape;
class BvhNode;
class Shader;
class Texture;
class Scene {

private:
	//making defaut constructor, copy constructor, assignment operators as private
	Scene() {
	}
	;
	Scene(Scene const&) {
	}
	;
	Scene& operator=(Scene const&) {
	}
	;
	std::vector<vishraylib::Camera*> cameras;
	std::vector<vishraylib::Shape*> shapes;
	std::map<string, Shader*> shaders;
	std::map<string, Texture*> textures;
	std::map<string, Shape*> baseObjects;
	std::vector<vishraylib::Light*> lights;
	BvhNode* root;
	static Scene* sceneInstance;
public:
	//vector<Shape*>* sortedObjects;
	static Scene * getInstance();
	std::vector<vishraylib::Camera*>& getCameras();
	std::vector<vishraylib::Shape*>& getShapes();
	Shape* getBaseObject(const string& baseObjectName);
	void putBaseObject(const string& baseObjectName, Shape* shape);
	Shader* getShader(const string& shaderName);
	void putShader(const string& shaderName, Shader* shader);
	Texture* getTexture(const string& name);
	void putTexture(const string& name, Texture* texture);
	std::vector<vishraylib::Light*>& getLights();
	Vector3D computeRayColor(Ray& ray, double tmin, double tmax, int depth,
			Vec2D& s);
	unsigned isNotInShadow(const Ray& shadowRay);
	Vector3D getBackground(Ray& r);
	map<string, Shape*> getBaseObjectMap();
	BvhNode* getRoot();
	void setRoot(BvhNode* root);
	void print();
};

//cosisder inlining: 
//		Vector3D computeRayColor(Ray& ray, double tmin, double tmax, int depth, Vec2D& s);
//		unsigned isNotInShadow(const Ray& shadowRay);
//		Vector3D getBackground(Ray& r);

/**
 * returns pointer to the singleton scene object
 *
 */
inline Scene* Scene::getInstance() {
	if (!sceneInstance)
		sceneInstance = new Scene;
	return sceneInstance;
}
}
#endif
