#if 0
#include "hitStruct.h"
#include "shader.h"
#include "light.h"
#include "scene.h"
#include "globals.h"
#include "iostream"
#include "shape.h"
using namespace vishraylib;
void HitStruct::applyShader(Ray& ray, double tmax, Vector3D& pixelColor) {
	Vector3D normal;
	Vector3D lightNormalized;
	Vector3D viewNormalized;
	Vector3D poi = ray.getOrigin()+tmax*ray.getDirection();

	Scene* scene = Scene::getInstance();
	Shader* shader = scene->getShader(shape->getShaderName());
	vector<Light*> lights = scene->getLights();
	vector<Shape*> shapes = scene->getShapes();
	shape->getNormal(poi,normal);
	viewNormalized = ray.getOrigin() - poi;
	viewNormalized.normalize();
	for(int i=0;i<lights.size();i++) {
		bool hit = false;
		Vector3D tempPixelColor;
		lightNormalized = *(lights[i]->getPosition())-poi;
		Ray shadowRay;
		shadowRay.setOrigin(poi);
		shadowRay.setDirection(*(lights[i]->getPosition())-poi);
		int k;
		double t_min = 1.0e-3;

		for(k =0;k<shapes.size();k++) {
			double t_max = 1;
			HitStruct hitInfo;
			hit=shapes[k]->intersect(shadowRay, t_min, t_max, hitInfo);
			if((hit)) {
				break;
			}

		}
		lightNormalized.normalize();
		if(!hit) {
			//	shader->applyShader(normal, lightNormalized, viewNormalized, *lights[i]->getIntensity(), tempPixelColor);
			//	pixelColor+=tempPixelColor;
		}
		if(args.verbose) {
			std::cout<<"normal "<<normal<<" lightNormalized "<<lightNormalized
			<<" viewNormalized "<<viewNormalized<<std::endl;
		}
	}
	pixelColor.clamp(0,1);
}

#endif
