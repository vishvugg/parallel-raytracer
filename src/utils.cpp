#include "utils.h"
#include "math.h"
#include "globals.h"
/**
 *
 * @param quadEqn the quadratic equation
 * @return descriminant for the quadratic equation
 */
float calcDescriminant(const sivelab::Vector3D& quadEqn) {
	return quadEqn[1] * quadEqn[1] - 4 * quadEqn[0] * quadEqn[2];
}

/**
 *
 * @param quadEqn quadratic equation
 * @param roots reference to roots for the equation
 * @return roots reference to roots for the equation
 */
bool calcQuadraticRoots(const sivelab::Vector3D& quadEqn, Vec2D& roots) {
	float descriminant = calcDescriminant(quadEqn);
	if (descriminant < 0) {
		return false;
	}
	if (args.verbose) {
		std::cout << "descriminant \t" << descriminant << "\t" << std::endl;
	}
	roots.x = (-quadEqn[1] + sqrt(descriminant)) / (2 * quadEqn[0]);
	roots.y = (-quadEqn[1] - sqrt(descriminant)) / (2 * quadEqn[0]);
	return true;
}
#if 0
float calcEpsilon() {
	float a = 1.0f;
	float eps = 1.0f;
	while ((a + eps) < a) {
		eps = eps * 0.5f;
	}
	return eps;
}
#endif

double max(double i, double j) {
	return i > j ? i : j;
}

double getElapsedSeconds(clock_t& start, int units) {
	return (std::clock() - start) / (double) (CLOCKS_PER_SEC / units);
}
