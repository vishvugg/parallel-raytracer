#ifndef VISH_RAY_LIB_LIGHT
#define VISH_RAY_LIB_LIGHT 1
#include "Vector3D.h"
#include "orthoNormalBasis.h"
#include "globals.h"
using namespace sivelab;
using namespace std;
namespace vishraylib {
//may need to abstract it
class Light {
public:
	Light() {
	}
	virtual ~Light() {
	}
	Light(Vector3D& p_position, Vector3D& p_intensity) :
			position(p_position), intensity(p_intensity) {

	}
	virtual void print()=0;
	virtual Vector3D getPosition(Vec2D& s)=0;
	virtual Vector3D getIntensity()=0;
protected:
	Vector3D position, intensity;
	//string type;
};

class PointLight: public Light {

public:
	PointLight(Vector3D& p_position, Vector3D& p_intensity) :
			Light(p_position, p_intensity) {
		//print();
	}
	Vector3D getPosition(Vec2D& s);
	Vector3D getIntensity();
	void print();
};
/**
 * Areal light class
 */
class ArealLight: public Light {

public:
	ArealLight(Vector3D& p_position, Vector3D& p_intensity, Vector3D& p_normal,
			double p_length, double p_width) :
			Light(p_position, p_intensity), normal(p_normal), length(p_length), width(
					p_width) {
		//print();
		basis = new OrthoNormalBasis(normal);
	}
	Vector3D getPosition(Vec2D& s);
	Vector3D getIntensity();
	void print();
private:
	double length;
	double width;
	Vector3D normal;
	OrthoNormalBasis* basis;
};

}
#endif
