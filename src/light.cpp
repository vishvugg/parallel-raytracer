#include "light.h"
using namespace vishraylib;
void PointLight::print() {
	cout << "light: " << std::endl << "\t position: " << position << std::endl
			<< "\t intensity: " << intensity << std::endl << std::endl;
}

Vector3D PointLight::getIntensity() {
	return intensity;
}

Vector3D PointLight::getPosition(Vec2D& s) {
	return position;
}

void ArealLight::print() {
	cout << "light: " << std::endl << "\tposition: " << position << std::endl
			<< "\tintensity: " << intensity << std::endl << "\tnormal: "
			<< normal << std::endl << "\tlength: " << length << std::endl
			<< "\twidth: " << width << std::endl;
}

Vector3D ArealLight::getPosition(Vec2D& s) {
	double r = width / 2.0;
	double l = -r;
	double t = length / 2.0;
	double b = -t;
	//get the position at bottom left
	Vector3D newPosition = position + l * basis->getU() + b * basis->getV();
	//compute new random point
	newPosition = newPosition + width * s.x * basis->getU()
			+ length * s.y * basis->getV();
	return newPosition;
}

Vector3D ArealLight::getIntensity() {
	return intensity;
}
