#ifndef VISH_RAY_LIB_HIT_STRUCT
#define VISH_RAY_LIB_HIT_STRUCT 1
#include <iostream>
#include "Vector3D.h"
#include "ray.h"
#include "shape.h"
#include "shader.h"
namespace vishraylib {
class Shape;
class Shader;
class HitStruct {
public:
	Shape* shape; //error: shape does not name a type
	Vector3D normal;//added normal here to work around mesh
	Shader* shader;//added shader here to work around mesh
	//included for debugging purposes
	string shaderName;
	HitStruct() :
			shape(NULL), shader(NULL) {
	}
};
}
#endif
