#include <iostream>
#include <cmath>
#include <boost/math/constants/constants.hpp>
#include <algorithm>

#include "shape.h"
#include "utils.h"
#include "globals.h"
#include "scene.h"

using namespace vishraylib;
using namespace std;



void Sphere::print() {

	cout << "Shape: Sphere" << std::endl << "\t name: " << name << std::endl
			<< "\t center: " << center << std::endl << "\t radius: " << radius
			<< std::endl << "\t shader: " << shaderName << std::endl
			<< std::endl;

}


/**
 * Computes the S and T values for a given point
 * @param poi - point
 * @return S and T values as Vec2D
 */
Vec2D Sphere::getS_And_T(Vector3D& poi) {
	const double pi = boost::math::constants::pi<double>();
	//hack
	double theta = acos((poi[1] - center[1]) / radius);
	double phi = atan2(poi[2] - center[2], poi[0] - center[0]);
	//cout<<radius<<", "<<poi-center<<", [ "<<theta<<", "<<phi<<" ]"<<endl;
	if (phi < 0)
		phi = phi + (2 * pi);
	//std::cout<<"poi"<<poi<<std::endl;
	//std::cout<<"theta "<<theta<<", phi"<<phi<<std::endl;
	//std::cout<<"theta "<<theta<<", phi"<<phi<<std::endl;
	//std::cout<<"theta "<<theta*180/pi<<", phi"<<phi*180/pi<<std::endl;
	double u = phi / (2 * pi);
	double v = (pi - theta) / pi;

	Vec2D s_and_t;
	s_and_t.x = u;
	s_and_t.y = v;
	return s_and_t;
}
/**
 *
 * @param ray reference to the ray with which objects are intersecting
 * @param tmin image plane position, defaults to 1
 * @param tmax distance from the image plane to the nearest object so far
 * @param hitStruct structure containing information of the nearest object so far
 * @return boolean indicating whether the ray hits the sphere
 */
bool Sphere::intersect(const Ray& ray, const double tmin, double& tmax,
		HitStruct& hitStruct) {
	leafHits++;
	Vector3D rayOrigin = ray.getOrigin();
	Vector3D rayDir = ray.getDirection();
	Vector3D eqn(rayDir.dot(rayDir), 2 * rayDir.dot(rayOrigin - center),
			(rayOrigin - center).dot(rayOrigin - center) - radius * radius);
	Vec2D roots;
	bool roots_exists = calcQuadraticRoots(eqn, roots);
	if (roots_exists) {
		roots_exists = false;
		//to check if roots lie between tmin, tmax
		if (roots.x > tmin || roots.y > tmin) {
			//std::cout << "Intersecting " << name << std::endl;

			if (roots.x < tmax) {
				tmax = roots.x;
				//std::cout << "tmax updated to " << tmax << std::endl;
				hitStruct.shape = this;
				hitStruct.shaderName = shaderName;
				hitStruct.shader = Scene::getInstance()->getShader(shaderName);
				Vector3D poi = ray.getOrigin() + tmax * ray.getDirection();
				Vector3D normal;
				getNormal(poi, normal);
				hitStruct.normal = normal;
				roots_exists = true;
			}

			if (roots.y < tmax) {
				tmax = roots.y;
				//std::cout << "tmax updated to " << tmax << std::endl;
				hitStruct.shape = this;
				hitStruct.shaderName = shaderName;
				hitStruct.shader = Scene::getInstance()->getShader(shaderName);
				Vector3D poi = ray.getOrigin() + tmax * ray.getDirection();
				Vector3D normal;
				getNormal(poi, normal);
				hitStruct.normal = normal;
				roots_exists = true;
			}
		}
	}
	//	std::cout << "rayOrigin:" << rayOrigin << "\t" << "rayDirection:"
	//			<< rayDir << "\teqn:" << eqn << "\t" << "roots:" << roots.x
	//			<< ", " << roots.y << std::endl;
	return roots_exists;
}

void Box::print() {
	cout << "Shape: Box " << std::endl << "\t name: " << name << std::endl
			<< "\t minPt: " << minPt << std::endl << "\t maxPt: " << maxPt
			<< std::endl << "\t shader: " << shaderName << std::endl
			<< std::endl;
}


bool Box::intersect(const Ray& ray, const double tmin, double& tmax,
		HitStruct& hitStruct) {
	leafHits++;
	bool result = false;
	for (int i = 0; i < 12; i++) {
		bool temp = true;
		temp = triangles[i]->intersect(ray, tmin, tmax, hitStruct);
		result |= temp;
		//if(temp){
		//std::cout<<"Box intersected"<<std::endl;
		//std::cout<<triangles[i]->getName()<<std::endl;
		//}
	}
	return result;
}

Vec2D Box::getS_And_T(Vector3D& poi) {
	//dummy it is never called, triangle is the one that is called
	Vec2D v;
	v.x = 0;
	v.y = 0;
	return v;
}

/**
 * Computes S and T values for a given point in the triangle
 * @param poi - a point on the triangle
 * @return S and T values in a Vec2D
 */
Vec2D Triangle::getS_And_T(Vector3D& poi) {
	Vec2D v;
	Vector3D v0 = this->v0.getCoordinates();
	Vector3D v1 = this->v1.getCoordinates();
	Vector3D v2 = this->v2.getCoordinates();

	Vec2D a = this->v0.getS_And_T();
	Vec2D b = this->v1.getS_And_T();
	Vec2D c = this->v2.getS_And_T();

	//double areaofA_a = (v2-v1).cross(poi-v1).length();
	double areaofA_b = (v0 - v2).cross(poi - v2).length();
	;
	double areaofA_c = (v1 - v0).cross(poi - v0).length();
	;
	double areaofA = (v1 - v0).cross(v2 - v0).length();
	double beta = areaofA_b / areaofA;
	double gamma = areaofA_c / areaofA;
	double alpha = 1 - beta - gamma;
	double s = a.x + beta * (b.x - a.x) + gamma * (c.x - a.x);
	double t = a.y + beta * (b.y - a.y) + gamma * (c.y - a.y);
	v.x = s;
	v.y = t;
	return v;
}

void Triangle::print() {
	Vector3D v0 = this->v0.getCoordinates();
	Vector3D v1 = this->v1.getCoordinates();
	Vector3D v2 = this->v2.getCoordinates();
	cout << "Shape: Triangle" << std::endl << "\t name: " << name << std::endl
			<< "\t vertices: " << v0 << ", " << v1 << ", " << v2 << std::endl
			<< "\t shader: " << shaderName << std::endl << std::endl;
}

void Triangle::setHitStruct(Vector3D& poi, HitStruct& hitStruct){
	hitStruct.shape = this;
	Vector3D normal;
	getNormal(poi, normal);
	hitStruct.normal = normal;
	hitStruct.shaderName = shaderName;
	hitStruct.shader = Scene::getInstance()->getShader(shaderName);
}

bool InstanceObject::intersect(const Ray& ray, const double tmin, double& tmax,
		HitStruct& hitStruct) {

	leafHits++;
	Ray invertedRay;
	invertedRay.setOrigin(
			inverseTransformationMatrix.multiply(ray.getOrigin(), 1));
	invertedRay.setDirection(
			inverseTransformationMatrix.multiply(ray.getDirection(), 0));
	if (baseObjPtr->intersect(invertedRay, tmin, tmax, hitStruct)) {
		//hitStruct.shape = this;
		hitStruct.normal = inverseTransformationMatrix.transpose().multiply(
				hitStruct.normal, 0);
		hitStruct.normal.normalize();
		hitStruct.shaderName = shaderName;
		hitStruct.shader = Scene::getInstance()->getShader(shaderName);
		//std::cout<<"Intsersected shape name: "<<name<<std::endl;
		//std::cout<<"shader name: "<<shaderName<<std::endl;
		return true;
	}
	return false;

}
//Dummy implementation
void InstanceObject::getNormal(Vector3D& poi, Vector3D& normal) {

}
void InstanceObject::print() {

	bbox.print();

}
Vec2D InstanceObject::getS_And_T(Vector3D& poi) {
	return baseObjPtr->getS_And_T(poi);
}

//Mesh implementation
bool OBJMesh::intersect(const Ray& ray, const double tmin, double& tmax,
		HitStruct& hitStruct) {

	if (tmin == 1.0e-3) {
		if (args.splitMethod == "split") {
			return root->shadowRayIntersect(ray, tmin, tmax, hitStruct);
		} else {
			for (int i = 0; i < triangles.size(); i++) {
				if (triangles[i]->intersect(ray, tmin, tmax, hitStruct))
					return true;
			}
			return false;
		}
	}

	double old_tmax = tmax;
	if (args.splitMethod == "split") {
		root->intersect(ray, tmin, tmax, hitStruct);
	} else {
		for (int i = 0; i < triangles.size(); i++) {
			triangles[i]->intersect(ray, tmin, tmax, hitStruct);
		}
	}
	//change hitstructure shape to this for getting shading information
	//adding actual triangle name for debugging purposes
	//tmax<old_tmax check is required 
	//When no triangle is intersected in the mesh 
	//The already closest object should not be over ridden by the mesh shader
	if (hitStruct.shape != NULL && tmax < old_tmax) {
		//cout<<"current tmax: "<<tmax<<endl;
		hitStruct.shaderName = shaderName + "_" + hitStruct.shape->getName();
		hitStruct.shape = this;
		hitStruct.shader = Scene::getInstance()->getShader(shaderName);
		return true;
	}
	return false;
}

void OBJMesh::print() {
	bbox.print();
}

//never called	
void OBJMesh::getNormal(Vector3D& poi, Vector3D& normal) {

}

Vec2D OBJMesh::getS_And_T(Vector3D& poi) {

}

bool BvhNode::shadowRayIntersect(const Ray& ray, const double tmin, double& tmax,
		HitStruct& hitStruct) {
	if (bbox.intersect(ray)) {
		bool leftHit = (left != NULL)
				&& left->shadowRayIntersect(ray, tmin, tmax, hitStruct);
		if(leftHit)
					return true;
		bool rightHit = (right != NULL)
				&& right->shadowRayIntersect(ray, tmin, tmax, hitStruct);
		if(rightHit)
					return true;
		return false;
	}
	return false;
}

inline bool compareX(Shape* v1, Shape* v2) {
	return v1->getBoundingBox().getXCenter()
			< v2->getBoundingBox().getXCenter();
}

inline bool compareY(Shape* v1, Shape* v2) {
	return v1->getBoundingBox().getYCenter()
			< v2->getBoundingBox().getYCenter();
}

inline bool compareZ(Shape* v1, Shape* v2) {
	return v1->getBoundingBox().getZCenter()
			< v2->getBoundingBox().getZCenter();
}

/**
 *
 * @param objects
 * @param axis
 * @param levels levels is used for debugging purposes
 */
//must construct a tree only for base objects not instanced objects
BvhNode::BvhNode(vector<Shape*>& objects, int axis, int levels) {
	int N = objects.size();
	if (N == 1) {
		left = objects[0];
		right = NULL;
		bbox = objects[0]->getBoundingBox();
		//Scene::getInstance()->sortedObjects.push_back(*objects[0]);
		//size+=sizeof(*objects[0]);

		//lnode = rnode = NULL;
	} else if (N == 2) {
		left = objects[0];
		right = objects[1];
		bbox = BoundingBox::combine(objects[0]->getBoundingBox(),
				objects[1]->getBoundingBox());
		//Scene::getInstance()->sortedObjects->push_back(*objects[0]);
	  //Scene::getInstance()->sortedObjects->push_back(*objects[1]);
		//size+=sizeof(*objects[0]);
		//size+=sizeof(*objects[1]);
		//
		//lnode = rnode = NULL;
		//lnode = rnode = NULL;
	} else {
		//sort elements
		//try using function pointers
		if (axis == 0) {
			sort(objects.begin(), objects.end(), compareX);
		} else if (axis == 1) {
			sort(objects.begin(), objects.end(), compareY);
		} else if (axis == 2) {
			sort(objects.begin(), objects.end(), compareZ);
		}
		vector<Shape*>::iterator mid = objects.begin() + N / 2 + 1;
		vector<Shape*> leftSubtreeObjects;
		leftSubtreeObjects.assign(objects.begin(), mid);
		vector<Shape*> rightSubtreeObjects;
		rightSubtreeObjects.assign(mid, objects.end());

		//lnode = new BvhNode(leftSubtreeObjects, (axis+1) % 3, levels+1);
		//rnode = new BvhNode(rightSubtreeObjects, (axis+1) % 3, levels+1);

		left = new BvhNode(leftSubtreeObjects, (axis + 1) % 3, levels + 1);
		right = new BvhNode(rightSubtreeObjects, (axis + 1) % 3, levels + 1);

		//left = lnode;
		//right = rnode;
		bbox = BoundingBox::combine(left->getBoundingBox(),
				right->getBoundingBox());
		//lnode = rnode = NULL;
	}
	level = levels;
}

