#ifndef VISH_RAY_LIB_RAY
#define VISH_RAY_LIB_RAY 1
#include "Vector3D.h"
using namespace sivelab;
namespace vishraylib {
/**
 * Class for a Ray
 * A ray has an origin and direction
 */
class Ray {
public:
	Ray() {
	}
	Ray(Vector3D& p_origin, Vector3D& p_direction) :
			origin(p_origin), direction(p_direction) {
	}
	~Ray() {
	}
	void setOrigin(const Vector3D& p_origin) {
		origin = p_origin;
	}
	const Vector3D& getOrigin() const {
		return origin;
	}
	void setDirection(const Vector3D& p_origin) {
		direction = p_origin;
	}
	const Vector3D& getDirection() const {
		return direction;
	}
	void print() {
		std::cout << "origin: " << origin << "," << "direction: " << direction
				<< "   " << std::endl;
	}
private:
	Vector3D origin, direction;

};
}
#endif
