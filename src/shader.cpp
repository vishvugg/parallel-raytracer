#include "shader.h"
#include <stdio.h>
#include <cmath>
#include "utils.h"
#include "globals.h"
#include "iostream"

using namespace vishraylib;
using namespace std;

void Mirror::print() {
	cout << "Shader: Mirror " << std::endl << "\t name: " << name << std::endl
			<< std::endl;
}

void Lambertian::print() {
	HitStruct hitStruct;
	Vector3D poi;
	cout << "Shader: Lambertian" << std::endl << "\t name: " << name
			<< std::endl << "\t diffuse->"
			<< Scene::getInstance()->getTexture(diffuse)->textureLookup(
					hitStruct, poi) << std::endl << std::endl;
}

void Glaze::print() {
	HitStruct hitStruct;
	Vector3D poi;
	cout << "Shader: Glaze" << std::endl << "\t name: " << name << std::endl
			<< "\t diffuse->"
			<< Scene::getInstance()->getTexture(diffuse)->textureLookup(
					hitStruct, poi) << std::endl << "\t mirrorCoef "
			<< mirrorCoef << std::endl << std::endl;
}

void Blinnphong::print() {
	HitStruct hitStruct;
	Vector3D poi;
	cout << "Shader: Blinnphong" << std::endl << "\t name: " << name
			<< std::endl << "\t diffuse->"
			<< Scene::getInstance()->getTexture(diffuse)->textureLookup(
					hitStruct, poi) << std::endl << "\t sepcular "
			<< Scene::getInstance()->getTexture(specular)->textureLookup(
					hitStruct, poi) << std::endl << phongExp << std::endl
			<< std::endl;
}

void BlinnPhongMirrored::print() {
	HitStruct hitStruct;
	Vector3D poi;
	cout << "Shader: BlinnPhongMirrored" << std::endl << "\t name: " << name
			<< std::endl << "\t diffuse->"
			<< Scene::getInstance()->getTexture(diffuse)->textureLookup(
					hitStruct, poi) << std::endl << "\t sepcular "
			<< Scene::getInstance()->getTexture(specular)->textureLookup(
					hitStruct, poi) << std::endl << "\t phongExp " << phongExp
			<< std::endl << "\t mirrorCoef " << mirrorCoef << std::endl
			<< std::endl;
}
/**
 *
 * @param ray
 * @param tmin
 * @param tmax
 * @param depth
 * @param hitStruct
 * @param s random vector used to get position in areal lights
 * @return
 */
Vector3D Mirror::applyShade(Ray& ray, double tmin, double tmax, int depth,
		HitStruct& hitStruct, Vec2D& s) {
	Vector3D color(0, 0, 0);
	Scene* scene = Scene::getInstance();
	Vector3D poi = ray.getOrigin() + tmax * ray.getDirection();

	Ray reflectedRay;
	reflectedRay.setOrigin(poi);

	Vector3D normal;
	Vector3D viewNormalized;
	normal = hitStruct.normal;
	viewNormalized = ray.getOrigin() - poi;
	viewNormalized.normalize();
	Vector3D reflectedRayDirection = viewNormalized * -1
			+ 2 * (viewNormalized.dot(normal)) * normal;
	reflectedRay.setDirection(reflectedRayDirection);
	if (roughness) {
		//:cout<<reflectedRayDirection<<endl;
		OrthoNormalBasis basis(reflectedRayDirection);
		float u = -roughness / 2.0 + s.x * roughness;
		float v = -roughness / 2.0 + s.y * roughness;
		reflectedRay.setDirection(
				reflectedRay.getDirection() + u * basis.getU()
						+ v * basis.getV());
	}
	double reflectedRay_tmin = 1.0e-3;
	double reflectedRay_tmax = numeric_limits<double>::max();
	//std::cout<<"mirror normal: "<<normal<<" view: "<<viewNormalized<<std::endl;	
	//std::cout << "mirror Ray" << std::endl;
	return scene->computeRayColor(reflectedRay, reflectedRay_tmin,
			reflectedRay_tmax, depth - 1, s);
}

/**
 *
 * @param ray
 * @param tmin
 * @param tmax
 * @param depth
 * @param hitStruct
 * @param s random vector used to get position in areal lights
 * @return
 */
Vector3D Lambertian::applyShade(Ray& ray, double tmin, double tmax, int depth,
		HitStruct& hitStruct, Vec2D& s) {

	Vector3D normal;
	Vector3D viewNormalized;
	Vector3D poi = ray.getOrigin() + tmax * ray.getDirection();

	Scene* scene = Scene::getInstance();
	vector<Light*> lights = scene->getLights();
	//vector<Shape*> shapes = scene->getShapes();
	normal = hitStruct.normal;
	viewNormalized = ray.getOrigin() - poi;
	viewNormalized.normalize();
	Vector3D color(0, 0, 0);
	for (int i = 0; i < lights.size(); i++) {
		Vector3D lightNormalized = (lights[i]->getPosition(s)) - poi;
		Ray shadowRay;
		shadowRay.setOrigin(poi);
		shadowRay.setDirection((lights[i]->getPosition(s)) - poi);
		lightNormalized.normalize();
		Vector3D intensity = lights[i]->getIntensity();
		Texture* diffuseTexture = Scene::getInstance()->getTexture(diffuse);
		Vector3D textureColor = diffuseTexture->textureLookup(hitStruct, poi);
		Vector3D L = (textureColor * intensity
				* max(0, normal.dot(lightNormalized)));
		//std::cout<<"Lambertian "<<L<<std::endl;
		int shadow = scene->getInstance()->isNotInShadow(shadowRay);
		//std::cout<<normal<<"    "<<lightNormalized<<std::endl;
		if (shadow) {
			color += shadow * L;
		}
		//compute shadow Ray
		//compute shading color
		//compute mirror ray
	}
	return color;
}
/**
 *
 * @param ray
 * @param tmin
 * @param tmax
 * @param depth
 * @param hitStruct
 * @param s random vector used to get position in areal lights
 * @return
 */
Vector3D Glaze::applyShade(Ray& ray, double tmin, double tmax, int depth,
		HitStruct& hitStruct, Vec2D& s) {

	Vector3D normal;
	Vector3D viewNormalized;
	Vector3D poi = ray.getOrigin() + tmax * ray.getDirection();

	Scene* scene = Scene::getInstance();
	vector<Light*> lights = scene->getLights();
	normal = hitStruct.normal;
	viewNormalized = ray.getOrigin() - poi;
	viewNormalized.normalize();
	Vector3D colorLambertian(0, 0, 0);
	for (int i = 0; i < lights.size(); i++) {
		Vector3D lightNormalized = (lights[i]->getPosition(s)) - poi;
		Ray shadowRay;
		shadowRay.setOrigin(poi);
		shadowRay.setDirection((lights[i]->getPosition(s)) - poi);
		lightNormalized.normalize();
		Vector3D intensity = lights[i]->getIntensity();
		Texture* diffuseTexture = Scene::getInstance()->getTexture(diffuse);
		Vector3D L = (diffuseTexture->textureLookup(hitStruct, poi) * intensity
				* max(0, normal.dot(lightNormalized)));
		//std::cout<<"Lambertian "<<L<<std::endl;
		int shadow = scene->getInstance()->isNotInShadow(shadowRay);
		//std::cout<<shadow<<std::endl;
		if (shadow) {
			colorLambertian += shadow * L;
		}
	}

	Ray reflectedRay;
	reflectedRay.setOrigin(poi);
	Vector3D reflectedRayDirection = viewNormalized * -1
			+ 2 * (viewNormalized.dot(normal)) * normal;
	reflectedRay.setDirection(reflectedRayDirection);
	if (roughness) {
		//:cout<<reflectedRayDirection<<endl;
		OrthoNormalBasis basis(reflectedRayDirection);
		float u = -roughness / 2.0 + s.x * roughness;
		float v = -roughness / 2.0 + s.y * roughness;
		reflectedRay.setDirection(
				reflectedRay.getDirection() + u * basis.getU()
						+ v * basis.getV());
	}
	double reflectedRay_tmin = 1.0e-3;
	double reflectedRay_tmax = numeric_limits<double>::max();
	return colorLambertian
			+ mirrorCoef
					* scene->computeRayColor(reflectedRay, reflectedRay_tmin,
							reflectedRay_tmax, depth - 1, s);
}
/**
 * @param ray
 * @param tmin
 * @param tmax
 * @param depth
 * @param hitStruct
 * @param s random vector used to get position in areal lights
 * @return
 */
Vector3D Blinnphong::applyShade(Ray& ray, double tmin, double tmax, int depth,
		HitStruct& hitStruct, Vec2D& s) {

	Vector3D normal;
	Vector3D viewNormalized;
	Vector3D poi = ray.getOrigin() + tmax * ray.getDirection();

	Scene* scene = Scene::getInstance();
	vector<Light*> lights = scene->getLights();
	//vector<Shape*> shapes = scene->getShapes();
	normal = hitStruct.normal;
	viewNormalized = ray.getOrigin() - poi;
	viewNormalized.normalize();
	Vector3D color(0, 0, 0);
	for (int i = 0; i < lights.size(); i++) {
		Vector3D lightNormalized = (lights[i]->getPosition(s)) - poi;
		Ray shadowRay;
		shadowRay.setOrigin(poi);
		shadowRay.setDirection((lights[i]->getPosition(s)) - poi);
		//normalize light only after computing shadow ray
		lightNormalized.normalize();
		Vector3D intensity = lights[i]->getIntensity();
		Vector3D halfVector = viewNormalized + lightNormalized;
		halfVector.normalize();
		Texture* diffuseTexture = Scene::getInstance()->getTexture(diffuse);
		Texture* specularTexture = Scene::getInstance()->getTexture(specular);
		Vector3D diffuseCoeff = diffuseTexture->textureLookup(hitStruct, poi);
		Vector3D specularCoeff = specularTexture->textureLookup(hitStruct, poi);
		//std::cout<<"diffuseCoeff "<<diffuseCoeff<<std::endl;
		//std::cout<<"specularCoeff "<<specularCoeff<<std::endl;
		Vector3D L = specularCoeff * intensity
				* pow(max(0, normal.dot(halfVector)), phongExp)
				+ (diffuseCoeff * intensity
						* max(0, normal.dot(lightNormalized)));
		//std::cout<<"diffuseCoeff "<<diffuseCoeff<<std::endl;
		//std::cout<<"specularCoeff "<<specularCoeff<<std::endl;
		int shadow = scene->getInstance()->isNotInShadow(shadowRay);
		if (shadow) {
			color += shadow * L;
		}
	}
	return color;
}

/**
 * @param ray
 * @param tmin
 * @param tmax
 * @param depth
 * @param hitStruct
 * @param s random vector used to get position in areal lights
 * @return
 */
Vector3D BlinnPhongMirrored::applyShade(Ray& ray, double tmin, double tmax,
		int depth, HitStruct& hitStruct, Vec2D& s) {
	Vector3D normal;
	Vector3D viewNormalized;
	Vector3D poi = ray.getOrigin() + tmax * ray.getDirection();

	Scene* scene = Scene::getInstance();
	vector<Light*> lights = scene->getLights();
	//vector<Shape*> shapes = scene->getShapes();
	normal = hitStruct.normal;
	viewNormalized = ray.getOrigin() - poi;
	viewNormalized.normalize();
	Vector3D colorBlinnphong(0, 0, 0);
	for (int i = 0; i < lights.size(); i++) {
		Vector3D lightNormalized = (lights[i]->getPosition(s)) - poi;
		Ray shadowRay;
		shadowRay.setOrigin(poi);
		shadowRay.setDirection((lights[i]->getPosition(s)) - poi);
		//normalize light only after computing shadow ray
		lightNormalized.normalize();
		Vector3D intensity = lights[i]->getIntensity();
		Vector3D halfVector = viewNormalized + lightNormalized;
		halfVector.normalize();
		Texture* diffuseTexture = Scene::getInstance()->getTexture(diffuse);
		Texture* specularTexture = Scene::getInstance()->getTexture(specular);
		Vector3D diffuseCoeff = diffuseTexture->textureLookup(hitStruct, poi);
		Vector3D specularCoeff = specularTexture->textureLookup(hitStruct, poi);

		Vector3D L = specularCoeff * intensity
				* pow(max(0, normal.dot(halfVector)), phongExp)
				+ (diffuseCoeff * intensity
						* max(0, normal.dot(lightNormalized)));
		//std::cout<<"Lambertian "<<L<<std::endl;
		int shadow = scene->getInstance()->isNotInShadow(shadowRay);
		if (shadow) {
			colorBlinnphong += shadow * L;
		}
	}
	Ray reflectedRay;
	reflectedRay.setOrigin(poi);
	Vector3D reflectedRayDirection = viewNormalized * -1
			+ 2 * (viewNormalized.dot(normal)) * normal;
	reflectedRay.setDirection(reflectedRayDirection);
	if (roughness) {
		//:cout<<reflectedRayDirection<<endl;
		OrthoNormalBasis basis(reflectedRayDirection);
		float u = -roughness / 2.0 + s.x * roughness;
		float v = -roughness / 2.0 + s.y * roughness;
		reflectedRay.setDirection(
				reflectedRay.getDirection() + u * basis.getU()
						+ v * basis.getV());
	}
	double reflectedRay_tmin = 1.0e-3;
	double reflectedRay_tmax = numeric_limits<double>::max();
	return (1 - mirrorCoef) * colorBlinnphong
			+ mirrorCoef
					* scene->computeRayColor(reflectedRay, reflectedRay_tmin,
							reflectedRay_tmax, depth - 1, s);
}
