#ifndef VISH_RAY_LIB_CAMERA
#define VISH_RAY_LIB_CAMERA 1
#include <string>
#include <iostream>
#include "Vector3D.h"
#include "orthoNormalBasis.h"
#include "ray.h"
#include "globals.h"
using namespace sivelab;
using namespace std;
namespace vishraylib {
/**
 * Abstract camera class
 */
class Camera {
public:

	enum CameraType {
		PERSPECTIVE, ORTHOGONAL
	};

	/**
	 *
	 * @param p_position - Camera's position
	 * @param p_viewDir - Camera's view Direction
	 * @param p_lookPoint - Camera's look at point
	 * @param p_imagePlaneWidth - Camera'sImage plane width
	 * @param p_focalLength - Camera' s Focal length
	 * @param p_name - Camera's name
	 * @param p_type - Type of the camera (Perscpective, Orthographic etc)
	 * @param p_lookatSet - look at point set or not?
	 */
	Camera(const Vector3D& p_position, const Vector3D& p_viewDir,
			const Vector3D& p_lookPoint, const float p_imagePlaneWidth,
			const float p_focalLength, const string& p_name, CameraType p_type,
			const bool p_lookatSet) :

			position(p_position), viewDir(p_viewDir), lookatPoint(p_lookPoint), imagePlaneWidth(
					p_imagePlaneWidth), focalLength(p_focalLength), name(
					p_name), type(p_type), lookatSet(p_lookatSet) {
		if (lookatSet)
			viewDir = lookatPoint - position;
		camUVW = new OrthoNormalBasis(viewDir, Vector3D(0, 1, 0));
		if (args.aspectRatio != 1)
			args.height = args.width / args.aspectRatio;
		r = imagePlaneWidth / 2;	//imagePlaneWidth is declared float
		l = -r;
		t = (imagePlaneWidth / (float(args.width) / float(args.height))) / 2;
		b = -t;
		//print();
		//camUVW->print();
	}

	virtual ~Camera() {
	}

	//defining print in cpp file caused a link error:/
	void print() {
		cout << "camera" << std::endl << "\tname: " << name << std::endl
				<< "\ttype: " << typeOfCamera() << std::endl << "\tposition: "
				<< position << std::endl;
		cout << "\tviewDir: " << viewDir << std::endl;
		if (lookatSet) {
			cout << "\tlookatPoint: " << lookatPoint << std::endl;
		}
		cout << "\tfocalLength: " << focalLength << std::endl;
		cout << "\timagePlaneWidth: " << imagePlaneWidth << std::endl
				<< std::endl;
		cout << "l = " << l << " r = " << r << " t = " << t << " b = " << b
				<< std::endl;
	}

	float getFocalLength() const;
	void setFocalLength(float focalLength);
	float getImagePlaneWidth() const;
	void setImagePlaneWidth(float imagePlaneWidth);
	const Vector3D& getLookatPoint() const;
	void setLookatPoint(const Vector3D& lookatPoint);
	const string& getName() const;
	void setName(const string& name);
	const Vector3D& getPosition() const;
	void setPosition(const Vector3D& position);
	CameraType getType() const;
	void setType(CameraType type);
	const Vector3D& getViewDir() const;
	void setViewDir(const Vector3D& viewDir);

	string typeOfCamera() const;
	float calcUCoefficient(double i);
	float calcVCoefficient(double j);
//	friend ostream& operator<<(ostream& out, const Camera& camera); 

	virtual void computeRay(Ray& ray, double i, double j)=0;
protected:
	Vector3D position, viewDir, lookatPoint;
	float imagePlaneWidth;
	float focalLength;
	string name;
	CameraType type;
	OrthoNormalBasis* camUVW;	//camera coordinate axes
	float l, r, t, b; // left, right, top, bottom edges on the image
	bool lookatSet;
};

/**
 * Perspective camera class
 */
class PerspectiveCamera: public Camera {
public:

	PerspectiveCamera(const Vector3D& p_position, const Vector3D& p_viewDir,
			const Vector3D& p_lookPoint, const float p_imagePlaneWidth,
			const float p_focalLength, const string& p_name, CameraType p_type,
			bool p_lookatSet) :
			Camera(p_position, p_viewDir, p_lookPoint, p_imagePlaneWidth,
					p_focalLength, p_name, p_type, p_lookatSet) {
	}

	~PerspectiveCamera() {
	}

	void computeRay(Ray& ray, double i, double j);

};
/**
 * Orthographic Camera class
 */
class OrthographicCamera: public Camera {
public:

	OrthographicCamera(const Vector3D& p_position, const Vector3D& p_viewDir,
			const Vector3D& p_lookPoint, const float p_imagePlaneWidth,
			const float p_focalLength, const string& p_name, CameraType p_type,
			bool p_lookatSet) :
			Camera(p_position, p_viewDir, p_lookPoint, p_imagePlaneWidth,
					p_focalLength, p_name, p_type, p_lookatSet) {
	}
	~OrthographicCamera() {
	}
	void computeRay(Ray& ray, double i, double j);

};

}
#endif
