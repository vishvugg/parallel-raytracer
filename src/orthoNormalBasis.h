#ifndef VISH_RAY_LIB_ORTHO_NORMAL_BASIS
#define VISH_RAY_LIB_ORTHO_NORMAL_BASIS 1

#include "Vector3D.h"

using namespace sivelab;
namespace vishraylib {
/**
 * class for Orthonormal basis
 */
class OrthoNormalBasis {
public:
	OrthoNormalBasis() {
		u = Vector3D(1, 0, 0);
		v = Vector3D(0, 1, 0);
		w = Vector3D(0, 0, 1);
	}

	OrthoNormalBasis(const Vector3D& a) {
		Vector3D* t;

		if (fabs(a[0]) < fabs(a[1]) && fabs(a[0]) < fabs(a[2]))
			t = new Vector3D(1, a[1], a[2]);
		else if (fabs(a[1]) < fabs(a[0]) && fabs(a[1]) < fabs(a[2]))
			t = new Vector3D(a[0], 1, a[2]);
		else
			//(fabs(a[2]) < fabs(a[0]) && fabs(a[2])<fabs(a[1]))
			t = new Vector3D(a[0], a[1], 1);
		w = a * -1;
		w.normalize();
		u = t->cross(w);
		u.normalize();
		v = w.cross(u);
#if 0		
		std::cout<<u<<std::endl;
		std::cout<<v<<std::endl;
		std::cout<<w<<std::endl;
		std::cout<<v.dot(w)<<std::endl;
		assert(u.dot(v)==0);
		assert(v.dot(w)==0);
		assert(w.dot(u)==0);
#endif
	}
	/**
	 * constructor when two vector are specified for constructing the basis
	 * @param gaze camera view direction
	 * @param b
	 */
	OrthoNormalBasis(const Vector3D& gaze, const Vector3D& b) {
		w = -1 * gaze;
		w.normalize();
		u = b.cross(w);
		u.normalize();
		v = w.cross(u);
#if 0
		std::cout<<u<<std::endl;
		std::cout<<v<<std::endl;
		std::cout<<w<<std::endl;
		std::cout<<v.dot(w)<<std::endl;
		assert(u.dot(v)==0);
		assert(v.dot(w)==0);
		assert(w.dot(u)==0);
#endif	
	}
	const Vector3D& getU() const {
		return u;
	}
	void setU(Vector3D& p_u) {
		u = p_u;
	}
	const Vector3D& getV() const {
		return v;
	}
	void setV(Vector3D& p_v) {
		v = p_v;
	}
	const Vector3D& getW() const {
		return w;
	}
	void setW(Vector3D& p_w) {
		w = p_w;
	}

	void print() {
		std::cout << "U" << getU() << std::endl << "V" << getV() << std::endl
				<< "W" << getW() << std::endl << std::endl;

	}
private:
	Vector3D u, v, w;

};

}

#endif
