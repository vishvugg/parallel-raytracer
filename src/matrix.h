#ifndef VISH_RAY_LIB_MATRIX
#define VISH_RAY_LIB_MATRIX 1

#include <vector>

#include "Vector3D.h"
using namespace sivelab;
using namespace std;

namespace vishraylib {
/**
 * Matrix class specific for transformations NOT a general purpose class
 */
class Matrix {

public:
	Matrix() {
	}
	Matrix(int num_rows, int num_columns, vector<double>& elements);
	//const Matrix getScaleMatrix();
	Matrix transpose();
	double determinant();
	Matrix inverse();
	Matrix buildMinor(int, int);
	Vector3D multiply(const Vector3D& vec, const int vectorType);
	Matrix multiply(const Matrix& m);
	friend std::ostream& operator<<(std::ostream& os, const Matrix &m);
	static Matrix getIdentityMatrix(int size);
	static Matrix getRotateX(float degreesOfRotation);
	static Matrix getRotateY(float degreesOfRotation);
	static Matrix getRotateZ(float degreesOfRotation);
	static Matrix getTranslate(float translateX, float translateY,
			float translateZ);
	static Matrix getScale(float scaleX, float scaleY, float scaleZ);
private:
	int num_rows;
	int num_columns;
	vector<vector<double> > rows;
	//vector<double> homogeneous_column_coordinates;
	//double homogeneous_col_x;
	//double homogeneous_col_y;
	//double homogeneous_col_z;

};

}
#endif
