#ifndef VISH_RAY_LIB_CREATORS
#define VISH_RAY_LIB_CREATORS 1

#include <iostream>
#include <sstream>
#include <cstdlib>
#include <cassert>
#include <stack>

#include "Vector3D.h"
#include "XMLSceneParser.h"
#include "handleGraphicsArgs.h"
#include "camera.h"
#include "scene.h"
#include "shape.h"
#include "shader.h"
#include "light.h"
#include "png++/png.hpp"
#include "textures.h"
#include "matrix.h"
#include "model_obj.h"
#include "utils.h"
#include "globals.h"

using namespace sivelab;
namespace vishraylib {
/**
 * Camera object instantiation from XML
 */
class CameraCreator: public SceneElementCreator {
public:
	CameraCreator() {
	}
	~CameraCreator() {
	}

	void instance(ptree::value_type const &v) {
		std::istringstream buf;

		std::string name, type;
		Vector3D position, viewDir, lookatPoint;
		bool lookatSet = false;
		float focalLength;
		float imagePlaneWidth;

		name = v.second.get<std::string>("<xmlattr>.name");
		type = v.second.get<std::string>("<xmlattr>.type");

		buf.str(v.second.get<std::string>("position"));
		buf >> position;
		buf.clear();

		// LookAtPoint and ViewDir are optional, but one should be specified.
		boost::optional<std::string> plookatPoint = v.second.get_optional<
				std::string>("lookatPoint");
		boost::optional<std::string> pviewDir = v.second.get_optional<
				std::string>("viewDir");

		if (plookatPoint) {
			lookatSet = true;
			buf.str(*plookatPoint);
			buf >> lookatPoint;
			buf.clear();
		} else if (pviewDir) {
			buf.str(*pviewDir);
			buf >> viewDir;
			buf.clear();
		}

		buf.str(v.second.get<std::string>("focalLength"));
		buf >> focalLength;
		buf.clear();

		buf.str(v.second.get<std::string>("imagePlaneWidth"));
		buf >> imagePlaneWidth;
		buf.clear();
		Scene* scene = Scene::getInstance();
		if (type.compare("perspective") == 0) {
			scene->getCameras().push_back(
					new PerspectiveCamera(position, viewDir, lookatPoint,
							imagePlaneWidth, focalLength, name,
							Camera::PERSPECTIVE, lookatSet));
		} else if (type.compare("orthogonal") == 0) {
			scene->getCameras().push_back(
					new OrthographicCamera(position, viewDir, lookatPoint,
							imagePlaneWidth, focalLength, name,
							Camera::ORTHOGONAL, lookatSet));
		}
	}

private:
};

class ContainerClass: public SceneElementCreator {
public:
	ContainerClass() {
	}
	~ContainerClass() {
	}

	void instance(ptree::value_type const &v) {
		//
		// Light
		//
		if (v.first == "light") {
			parseLightData(v);
		}

		//
		// Shader
		//
		if (v.first == "shader") {
			parseShaderData(v);
		}

		//
		// Shape or Instance
		//
		if (v.first == "shape") {
			parseShapeData(v);
		}
		//
		// Texture
		//
		if (v.first == "texture") {
			parseTextureData(v);
		}
		//
		//sceneParameters
		//
		if (v.first == "sceneParameters") {
			parseSceneParameters(v);
		}
		//
		//Instance
		//
		if (v.first == "instance") {
			parseShapeData(v);
		}
	}
	/**
	 * sceneParameters instantiation
	 */
	void parseSceneParameters(ptree::value_type const&v) {

		sivelab::Vector3D bgColor;
		std::istringstream buf;
		buf.str(v.second.get<std::string>("bgColor"));
		buf >> bgColor;
		boost::optional<std::string> envMapPrefix = v.second.get_optional<
				std::string>("envMapPrefix");
		if (envMapPrefix) {
			struct {
				void getImageAndCreateTexture(string& path, string dir) {
					Scene* scene = Scene::getInstance();
					png::image<png::rgb_pixel>* imData = new png::image<
							png::rgb_pixel>(path + dir + ".png");
					scene->putTexture("background_" + dir,
							new ImageTexture(imData));
				}
			} local;
			local.getImageAndCreateTexture(*envMapPrefix, "posX");
			local.getImageAndCreateTexture(*envMapPrefix, "negX");
			local.getImageAndCreateTexture(*envMapPrefix, "posY");
			local.getImageAndCreateTexture(*envMapPrefix, "negY");
			local.getImageAndCreateTexture(*envMapPrefix, "posZ");
			local.getImageAndCreateTexture(*envMapPrefix, "negZ");
		} else {
			Scene::getInstance()->putTexture("background",
					new StaticTexture(bgColor));
		}

	}
	/**
	 *	Texture object instantiation from XML 
	 */
	void parseTextureData(ptree::value_type const&v) {
		std::string type;
		std::string name;
		type = v.second.get<std::string>("<xmlattr>.type");
		name = v.second.get<std::string>("<xmlattr>.name");
		if (type == "image") {
			std::string path;
			path = v.second.get<std::string>("sourcefile");
			png::image<png::rgb_pixel>* imData = new png::image<png::rgb_pixel>(
					path);
			Scene::getInstance()->putTexture(name, new ImageTexture(imData));
		} else if (type == "PerlinNoise") {
			Scene::getInstance()->putTexture(name, new PerlinNoise());
		}
	}
	/**
	 * Light object instantiation from XML
	 */
	void parseLightData(ptree::value_type const &v) {
		std::string type;
		sivelab::Vector3D position, intensity;
		Scene* scene = Scene::getInstance();
		type = v.second.get<std::string>("<xmlattr>.type");
		std::istringstream buf;
		buf.str(v.second.get<std::string>("position"));
		buf >> position;
		buf.clear();

		buf.str(v.second.get<std::string>("intensity"));
		buf >> intensity;
		buf.clear();
		if (type == "area") {
			Vector3D normal;
			double length;
			double width;
			buf.str(v.second.get<std::string>("normal"));
			buf >> normal;
			buf.clear();

			width = v.second.get<double>("width");
			length = v.second.get<double>("length");
			scene->getLights().push_back(
					new ArealLight(position, intensity, normal, length, width));
			return;
		}
		scene->getLights().push_back(new PointLight(position, intensity));
	}
	/**
	 * Shape object instantiation from XML
	 */
	void parseShapeData(ptree::value_type const &v) {
		std::string type, name;
		type = v.second.get<std::string>("<xmlattr>.type");
		name = v.second.get<std::string>("<xmlattr>.name");
		//***************************************************	
		//
		//addition to store the shader name the shape refers to
		//
		//***************************************************
		string shaderName = "";
		//shaderName = v.second.get<std::string>("<xmlattr>.ref");
		// Make sure to find the shader data objects; HAVE to include a
		// shader in this version; Need to find the shader in the second part
		// of the shape pair
		ptree::const_assoc_iterator it = v.second.find("shader");
		if (it != v.second.not_found()) {
			const ptree::value_type &v = *it;
			boost::optional<std::string> optShaderRef = v.second.get_optional<
					std::string>("<xmlattr>.ref");
			shaderName = *optShaderRef;
			//std::cout << "Found optional shader ref: " << *optShaderRef << std::endl;
			parseShaderData(*it);
		}

		//retriving Shapes from scene
		Scene* scene = Scene::getInstance();
		Shape *s;
		std::istringstream buf;
		if (type == "instance") {
			std::stack<Matrix> matrixStack;
			string id = v.second.get<std::string>("<xmlattr>.id");
			s = scene->getBaseObject(id);
			ptree transform = v.second;
			//cout<<endl<<"name: "<<name<<endl;
			BOOST_FOREACH( ptree::value_type const &v, transform.get_child("transform")){
			//cout<<v.first<<endl;
			string transformationData = v.second.data();
			buf.str(transformationData);
			if(v.first=="rotate") {
				//cout<<"rotate: "<<transformationData;
				float degreesOfRotation;
				buf >> degreesOfRotation;
				OrthoNormalBasis basis();
				string rotationAxis = v.second.get<std::string>("<xmlattr>.axis");
				if(rotationAxis == "X") {
					//cout<<", axis: "<<rotationAxis<<endl;
					Matrix temp = Matrix::getRotateX(degreesOfRotation);
					//cout<<temp;
					matrixStack.push(temp);
				} else if(rotationAxis == "Y") {
					//cout<<", axis: "<<rotationAxis<<endl;
					Matrix temp = Matrix::getRotateY(degreesOfRotation);
					//cout<<temp;
					matrixStack.push(temp);
				} else if(rotationAxis == "Z") {
					//cout<<", axis: "<<rotationAxis<<endl;
					Matrix temp = Matrix::getRotateZ(degreesOfRotation);
					//cout<<temp;
					matrixStack.push(temp);
				}
				//cout<<degreesOfRotation<<endl;
			}
			//translate
			else if(v.first == "translate") {
				//cout<<"Translate: "<<transformationData<<endl;
				float translateX, translateY, translateZ;
				buf >> translateX;
				buf >> translateY;
				buf >> translateZ;
				//cout<<translateX<<", "<<translateY<<", "<<translateZ<<endl;
				Matrix temp = Matrix::getTranslate(translateX, translateY, translateZ);
				//cout<<temp;
				matrixStack.push(temp);
			}
			//scale
			else if(v.first == "scale") {
				//cout<<"scale: "<<transformationData<<endl;
				float scaleX, scaleY, scaleZ;
				buf >> scaleX;
				buf >> scaleY;
				buf >> scaleZ;
				//cout<<scaleX<<", "<<scaleY<<", "<<scaleZ<<endl;
				Matrix temp = Matrix::getScale(scaleX, scaleY, scaleZ);
				//cout<<temp;
				matrixStack.push(temp);
			}
			buf.clear();
		}
			Matrix transformationMatrix = Matrix::getIdentityMatrix(4);
			//cout<<"building transformation matrix: "<<endl;
			while (!matrixStack.empty()) {
				Matrix temp = matrixStack.top();
				matrixStack.pop();
				//cout <<"popped matrix: "<<endl<<temp;
				transformationMatrix = temp.multiply(transformationMatrix);
				//cout<<"transformation matrix: "<<endl<<transformationMatrix;
			}
			//cout<<"final transformation matrix"<<endl<<transformationMatrix;
			Matrix inverseTransformationMatrix = transformationMatrix.inverse();
			//cout<<"final inverse transformation matrix"<<endl<<inverseTransformationMatrix;
			Shape* shape = scene->getBaseObject(id);
			if (shaderName == "")
				shaderName = shape->getShaderName();
			s = new InstanceObject(name, shaderName, shape,
					inverseTransformationMatrix);

			//cout<<"Time to instance creation: "<<getElapsedSeconds(start)<<endl;
		}
		if (type == "sphere") {
			float radius;
			sivelab::Vector3D center;

			buf.str(v.second.get<std::string>("center"));
			buf >> center;
			buf.clear();

			radius = v.second.get<float>("radius");

			s = new Sphere(name, center, radius, shaderName);
		}

		if (type == "box") {
			sivelab::Vector3D minPt, maxPt;

			buf.str(v.second.get<std::string>("minPt"));
			buf >> minPt;
			buf.clear();

			buf.str(v.second.get<std::string>("maxPt"));
			buf >> maxPt;
			buf.clear();
			s = new Box(name, minPt, maxPt, shaderName);
			//cout<<"created box"<<name<<endl;
		}

		if (type == "triangle") {
			sivelab::Vector3D v0, v1, v2;

			buf.str(v.second.get<std::string>("v0"));
			buf >> v0;
			buf.clear();

			buf.str(v.second.get<std::string>("v1"));
			buf >> v1;
			buf.clear();

			buf.str(v.second.get<std::string>("v2"));
			buf >> v2;
			buf.clear();
			s = new Triangle(name, v0, v1, v2, shaderName);
		}

		if (type == "mesh") {
			string objFilePath = v.second.get<std::string>("file");
			//cout<<objFilePath;
			ModelOBJ mOBJ;
			if (!mOBJ.import(objFilePath.c_str())) {
				cout << "Unable to load obj file" << endl;
				exit(EXIT_FAILURE);
			}
			const ModelOBJ::Mesh *pMesh = 0;
			const ModelOBJ::Material *pMaterial = 0;
			const ModelOBJ::Vertex *pVertices = 0;

			const int *idxBuffer = mOBJ.getIndexBuffer();
			vector<Shape*> triangles;
			double negInfinity = NEG_INFINITY;
			double posInfinity = numeric_limits<double>::max();
			Vector3D bb_maxExtent(negInfinity, negInfinity, negInfinity);
			Vector3D bb_minExtent(posInfinity, posInfinity, posInfinity);
			for (int mIdx = 0; mIdx < mOBJ.getNumberOfMeshes(); mIdx++) {
				pMesh = &mOBJ.getMesh(mIdx);
				pMaterial = pMesh->pMaterial;
				pVertices = mOBJ.getVertexBuffer();

				//remove this 
				int numTris = 0;
				for (int i = pMesh->startIndex;
						i < (pMesh->startIndex + pMesh->triangleCount * 3); i +=
								3) {
					//if(numTris++==5)
					//				break;
					ModelOBJ::Vertex v0, v1, v2;
					v0 = pVertices[idxBuffer[i]];
					v1 = pVertices[idxBuffer[i + 1]];
					v2 = pVertices[idxBuffer[i + 2]];

					//read vertex coordinates
					Vector3D v0_coordinates(v0.position[0], v0.position[1],
							v0.position[2]);
					Vector3D v1_coordinates(v1.position[0], v1.position[1],
							v1.position[2]);
					Vector3D v2_coordinates(v2.position[0], v2.position[1],
							v2.position[2]);
					//read vertex normals
					stringstream sstream;
					sstream << i / 3;
					string triangleIndex = sstream.str();
					if (mOBJ.hasNormals()) {
						Vector3D v0_normal(v0.normal[0], v0.normal[1],
								v0.normal[2]);
						Vector3D v1_normal(v1.normal[0], v1.normal[1],
								v1.normal[2]);
						Vector3D v2_normal(v2.normal[0], v2.normal[1],
								v2.normal[2]);

						Vertex vertex0(v0_coordinates, v0_normal);
						Vertex vertex1(v1_coordinates, v1_normal);
						Vertex vertex2(v2_coordinates, v2_normal);

						//push the triangle into a list
						//need not include any shader name OBJMesh::intersectio() pushes
						//the appropriate shader
						triangles.push_back(
								new Triangle(name + "_" + triangleIndex,
										vertex0, vertex1, vertex2, "", true));
					} else {
						triangles.push_back(
								new Triangle(name + "_" + triangleIndex,
										v0_coordinates, v1_coordinates,
										v2_coordinates, ""));

					}

					bb_minExtent = BoundingBox::minExtent(bb_minExtent,
							v0_coordinates);
					bb_minExtent = BoundingBox::minExtent(bb_minExtent,
							v1_coordinates);
					bb_minExtent = BoundingBox::minExtent(bb_minExtent,
							v2_coordinates);

					bb_maxExtent = BoundingBox::maxExtent(bb_maxExtent,
							v0_coordinates);
					bb_maxExtent = BoundingBox::maxExtent(bb_maxExtent,
							v1_coordinates);
					bb_maxExtent = BoundingBox::maxExtent(bb_maxExtent,
							v2_coordinates);
					sstream.clear();

					//cout<<"Time to one triangle pass: "<<getElapsedSeconds(start, 1000)<<endl;
				}
			}
			BoundingBox bbox(bb_minExtent, bb_maxExtent);
			//To avoid a big computation again in mesh bounding box is created here itself and passed
			s = new OBJMesh(name, shaderName, triangles, bbox);
			if (v.first == "instance") {
				//cout<<"Base object: "<<name<<" created"<<endl;
				scene->putBaseObject(name, s);
			} else if (v.first == "shape") {
				scene->getShapes().push_back(s);
				//cout<<"pushing shape into shapes: "<<name<<endl;
			}
			//we may have lot of meshes and we push them into shapes earlier 
			//we just return because normally at the end of this method we push 
			//a shape into scene::shapes
			return;
		}

		if (v.first == "instance") {
			//cout<<"Base object: "<<name<<" created"<<endl;
			scene->putBaseObject(name, s);
		} else if (v.first == "shape") {
			scene->getShapes().push_back(s);
			//cout<<"pushing shape into shapes: "<<name<<endl;
		}

	}
	/**
	 * Shader object instantiation from XML
	 */
	void parseShaderData(ptree::value_type const &v) {

		Scene* scene = Scene::getInstance();

		std::istringstream buf;

		std::string type, name;
		boost::optional<std::string> optShaderRef = v.second.get_optional<
				std::string>("<xmlattr>.ref");

		if (!optShaderRef) {
			type = v.second.get<std::string>("<xmlattr>.type");
			name = v.second.get<std::string>("<xmlattr>.name");
		}
		//: else
		// std::cout << "Found optional shader ref: " << *optShaderRef << std::endl;

		// if name exists in the shader map and this is a ref, return the shader pointer
		// otherwise, add the shader if it NOT a ref and the name doesn't exist
		// final is to throw error
		if (!optShaderRef) {

			// Did not find the shader and it was not a reference, so create and return
			//std::cout<<"shader!";
			if (type == "Lambertian") {
				sivelab::Vector3D kd;
				buf.str(v.second.get<std::string>("diffuse"));
				buf >> kd;
				buf.clear();
				boost::optional<std::string> optTextureRef =
						v.second.get_optional<std::string>(
								"diffuse.<xmlattr>.tex");
				//std ::string  textureName= *optTextureRef;
				if (optTextureRef) {
					//if there is a texture store its name
					scene->putShader(name,
							new Lambertian(name, *optTextureRef));

				} else {
					//if there is no texture store the name of the shader as texture name
					//and create a static texture object
					scene->putShader(name,
							new Lambertian(name, name + "_diffuse"));
					scene->putTexture(name + "_diffuse", new StaticTexture(kd));
				}

			} else if (type == "BlinnPhong" || type == "Phong") {
				sivelab::Vector3D kd;
				sivelab::Vector3D ks;
				float phongExp;
				buf.str(v.second.get<std::string>("diffuse"));
				buf >> kd;
				buf.clear();
				boost::optional<std::string> optDiffuseTexRef =
						v.second.get_optional<std::string>(
								"diffuse.<xmlattr>.tex");

				//std::cout<<*optTextureRef<<std::endl;
				buf.str(v.second.get<std::string>("specular"));
				buf >> ks;
				buf.clear();
				boost::optional<std::string> optSpecularTexRef =
						v.second.get_optional<std::string>(
								"specular.<xmlattr>.tex");

				//std::cout<<*optTextureRef<<std::endl;
				phongExp = v.second.get<float>("phongExp");

				if (optDiffuseTexRef && optSpecularTexRef)
					scene->putShader(name,
							new Blinnphong(string(name),
									string(*optDiffuseTexRef),
									string(*optSpecularTexRef), phongExp));
				else {
					scene->putShader(name,
							new Blinnphong(name, name + "_diffuse",
									name + "_specular", phongExp));
					scene->putTexture(name + "_diffuse", new StaticTexture(kd));
					scene->putTexture(name + "_specular",
							new StaticTexture(ks));
				}
				//boost::optional<float> mirrorCoef =
				//		v.second.get_optional<float>("mirrorCoef");
				//if (mirrorCoef) {
				//	float km = mirrorCoef.get();
				//	scene->putShader(name,
				//			new BlinnPhongMirrored(name,  new StaticDiffuse(kd),  new StaticSpecular(ks), phongExp, km));
				//}

			} else if (type == "BlinnPhongMirrored") {
				sivelab::Vector3D kd;
				sivelab::Vector3D ks;
				float phongExp;
				buf.str(v.second.get<std::string>("diffuse"));
				buf >> kd;
				buf.clear();

				boost::optional<std::string> optDiffuseTexRef =
						v.second.get_optional<std::string>(
								"diffuse.<xmlattr>.tex");

				buf.str(v.second.get<std::string>("specular"));
				buf >> ks;
				buf.clear();

				float km = v.second.get<float>("mirrorCoef");
				boost::optional<std::string> optSpecularTexRef =
						v.second.get_optional<std::string>(
								"specular.<xmlattr>.tex");

				boost::optional<double> optRoughness = v.second.get_optional<
						double>("roughness");
				double roughness = 0;
				if (optRoughness)
					roughness = *optRoughness;

				//std::cout<<*optTextureRef<<std::endl;
				phongExp = v.second.get<float>("phongExp");
				//
				//
				//need to change it
				//
				//
				if (optDiffuseTexRef && optSpecularTexRef)
					scene->putShader(name,
							new BlinnPhongMirrored(string(name),
									string(*optDiffuseTexRef),
									string(*optSpecularTexRef), phongExp, km,
									roughness));
				else {
					scene->putShader(name,
							new BlinnPhongMirrored(name, name + "_diffuse",
									name + "_specular", phongExp, km,
									roughness));
					scene->putTexture(name + "_diffuse", new StaticTexture(kd));
					scene->putTexture(name + "_specular",
							new StaticTexture(ks));
				}
			} else if (type == "Glaze") {
				sivelab::Vector3D kd;
				buf.str(v.second.get<std::string>("diffuse"));
				buf >> kd;
				buf.clear();

				float km = v.second.get<float>("mirrorCoef");

				boost::optional<std::string> optTextureRef =
						v.second.get_optional<std::string>(
								"diffuse.<xmlattr>.tex");

				boost::optional<double> optRoughness = v.second.get_optional<
						double>("roughness");
				double roughness = 0;
				if (optRoughness)
					roughness = *optRoughness;

				//	scene->putShader(name, new Glaze(name,  new StaticDiffuse(kd), km));
				if (optTextureRef) {
					//if there is a texture store its name
					scene->putShader(name,
							new Glaze(name, *optTextureRef, km, roughness));

				} else {
					//if there is no texture store the name of the shader as texture name
					//and create a static texture object
					scene->putShader(name,
							new Glaze(name, name + "_diffuse", km, roughness));
					scene->putTexture(name + "_diffuse", new StaticTexture(kd));
				}
			} else if (type == "Mirror") {

				boost::optional<double> optRoughness = v.second.get_optional<
						double>("roughness");
				double roughness = 0;
				if (optRoughness)
					roughness = *optRoughness;

				scene->putShader(name, new Mirror(name, roughness));
			}
		}
	}

private:

};

}
#endif
